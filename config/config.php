<?php

//Array con la lista de controladores disponibles. Clave - Valor (nombre del archivo)
$controladores =[
    'inicio' => 'controller/cInicio.php',
    'login' => 'controller/cLogin.php',
    'infolibro' => 'controller/cInfoLibro.php',
    'paguser' => 'controller/cPagUser.php',
    'paguserperfil' => 'controller/cPagUserPerfil.php',
    'pagpago' => 'controller/cPagPago.php',
    'ver' => 'controller/cVer.php',
    'subir' => 'controller/cSubir.php',
    'modlibro' => 'controller/cModificarLibro.php'
];

//Array con la lista de vistas disponibles. Clave - Valor (nombre del archivo)
$vistas = [
    'inicio' => 'view/vInicio.php',
    'login' => 'view/vLogin.php',
    'infolibro' => 'view/vInfoLibro.php',
    'paguser' => 'view/vPagUser.php',
    'paguserperfil' => 'view/vPagUserPerfil.php',
    'pagpago' => 'view/vPagPago.php',
    'ver' => 'view/vVer.php',
    'subir' => 'view/vSubir.php',
    'modlibro' => 'view/vModificarLibro.php'
];

?>