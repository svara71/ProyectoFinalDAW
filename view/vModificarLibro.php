<header>
	
	<div id="inicio">
		<form method="post" action="index.php?location=inicio">
			<input type="submit" name="Inicio" value="" id="botonInicio">
		</form>	
	</div> 
	<div id="titulo"><h2>Inicio Sesión</h2></div>
	<div id="sesion">
		<form method="post" action="index.php?location=inicio">
			<?php 
				if(isset($_SESSION['usuario'])){
					if($_SESSION['usuario']->getAdmin()==1){
			?>
						<input type="submit" name="Perfil" value="" id="botonPerfil">
						<input type="submit" name="Cerrar" value="" id="botonCerrar">
			<?php 
					}else{
			?>	
						<input type="submit" name="PaginaUser" value="" id="botonUser">
						<input type="submit" name="Cerrar" value="" id="botonCerrar">
			<?php 
					}
				}else{
			?>
				<input type="submit" name="login" value="" id="botonSesion">
			<?php  
				}
			?>
			

		</form>	
	</div>
</header>

<section id="subir">
		<div id="contenido">
			<div id="formSubir">
				<form method="post" action="index.php?location=modlibro&referencia=<?php print_r($libro->getRef_Libro()) ?>" enctype="multipart/form-data">
						<h2>Modificar Libro</h2>

					Nombre del Libro:
					<span class='error'>
						<?php  
						//SI exite el error 
							if(isset($error['errorNomLibroUp'])){
								//Lo Mostramos
								print $error['errorNomLibroUp'];
							}
						?>
					</span>
					 <br>
					 <input type="text" name="nomLibroUp" value="<?php print_r($libro->getNom_Libro()) ?>">
					 <br>

					 ISBN:
					<span class='error'>
						<?php  
						//SI exite el error 
							if(isset($error['errorISBNUp'])){
								//Lo Mostramos
								print $error['errorISBNUp'];
							}
						?>
					</span>
					<br>
					<input type="text" name="isbnUp" value="<?php print_r($libro->getRef_Libro()) ?>" readonly>
					<br>

					 Autor:
					<span class="error">
						<?php  
							//SI exite el error 
							if(isset($error['errorAutorUp'])){
								//Lo Mostramos
								print $error['errorAutorUp'];
							}
						?>
					</span>
					<br>		
					<input type="text" name="autorUp" value="<?php print_r($libro->getAutor()) ?>">
				    <br>

					Numero de Paginas:
					<span class="error">
						<?php  
							//SI exite el error 
							if(isset($error['errorNumPagUp'])){
								//Lo Mostramos
								print $error['errorNumPagUp'];
							}
						?>
					</span>
					<br> 
					<input type="text" name="numPagUp" value="<?php print_r($libro->getNum_Pag()) ?>">
					<br>


					Precio:
					<span class="error">
						<?php  
						//SI exite el error 
							if(isset($error['errorPrecioUp'])){
								//Lo Mostramos
								print $error['errorPrecioUp'];
							}
						?>
					</span>
					<br>  
					<input type="text" name="precioUp" value="<?php print_r($libro->getPrecio()) ?>">
					<br>
					<br> 

					<span>Generos:(<span style="font-weight: normal;font-size: x-small;"><?php print_r($libro->getGeneros()) ?></span>)</span>
					 <span class="error">
					 	<?php
					 		//SI exite el error 
							if(isset($error['errorGenerosUp'])){
								//Lo Mostramos
								print $error['errorGenerosUp'];
							}
						?>
					</span>
					 <br>
					<div id="generos">
					 <br>
					

						<span><input type="checkbox" name="generosUp[]" value="Autoayuda" <?php if(strpos($libro->getGeneros(),'Autoayuda')!== false ){print('checked'); }?> >AUTOAYUDA</span>
						<span><input type="checkbox" name="generosUp[]" value="Aventura" <?php if(strpos($libro->getGeneros(),'Aventura')!== false ){print('checked'); }?> >AVENTURA</span>
						<span><input type="checkbox" name="generosUp[]" value="Biográfica" <?php if(strpos($libro->getGeneros(),'Biográfica')!== false ){print('checked'); }?> >BIOGRÁFICA</span>
						<span><input type="checkbox" name="generosUp[]" value="Ciencia Ficción" <?php if(strpos($libro->getGeneros(),'Ciencia Ficción')!== false ){print('checked'); }?> >CIENCIA FICCIÓN</span>
						<span><input type="checkbox" name="generosUp[]" value="Cultura" <?php if(strpos($libro->getGeneros(),'Cultura')!== false ){print('checked'); }?> >CULTURA</span>
						<span><input type="checkbox" name="generosUp[]" value="Deportes" <?php if(strpos($libro->getGeneros(),'Deportes')!== false){print('checked'); }?> >DEPORTES</span>
						<span><input type="checkbox" name="generosUp[]" value="Drama" <?php if(strpos($libro->getGeneros(),'Drama')!== false ){print('checked'); }?> >DRAMA</span>
						<span><input type="checkbox" name="generosUp[]" value="Economía" <?php if(strpos($libro->getGeneros(),'Economía')!== false){print('checked'); }?> >ECONOMÍA</span>
						<span><input type="checkbox" name="generosUp[]" value="Erótica" <?php if(strpos($libro->getGeneros(),'Erótica')!== false ){print('checked'); }?> >ERÓTICA</span>
						<span><input type="checkbox" name="generosUp[]" value="Fantasía" <?php if(strpos($libro->getGeneros(),'Fantasía')!== false ){print('checked'); }?> >FANTASIA</span>
						<span><input type="checkbox" name="generosUp[]" value="Infantil" <?php if(strpos($libro->getGeneros(),'Infantil')!== false ){print('checked'); }?> >INFANTIL</span>
						<span><input type="checkbox" name="generosUp[]" value="Investigación" <?php if(strpos($libro->getGeneros(),'Investigación')!== false ){print('checked'); }?> >INVESTIGACIÓN</span>
						<span><input type="checkbox" name="generosUp[]" value="Juvenil" <?php if(strpos($libro->getGeneros(),'Juvenil')!== false ){print('checked'); }?> >JUVENIL</span>
						<span><input type="checkbox" name="generosUp[]" value="Poesia" <?php if(strpos($libro->getGeneros(),'Poesia')!== false ){print('checked'); }?> >POESIA</span>
						<span><input type="checkbox" name="generosUp[]" value="Policial" <?php if(strpos($libro->getGeneros(),'Policial')!== false ){print('checked'); }?> >POLICIAL</span>
						<span><input type="checkbox" name="generosUp[]" value="Política" <?php if(strpos($libro->getGeneros(),'Política')!== false ){print('checked'); }?> >POLÍTICA</span>
						<span><input type="checkbox" name="generosUP[]" value="Romántica" <?php if(strpos($libro->getGeneros(),'Romántica')!== false ){print('checked'); }?> >ROMÁNTICA</span>
						<span><input type="checkbox" name="generosUp[]" value="Sociedad" <?php if(strpos($libro->getGeneros(),'Sociedad')!== false ){print('checked'); }?> >SOCIEDAD</span>
						<span><input type="checkbox" name="generosUp[]" value="Terror" <?php if(strpos($libro->getGeneros(),'Terror')!== false ){print('checked'); }?> >TERROR</span>
						<span><input type="checkbox" name="generosUp[]" value="Viajes" <?php if(strpos($libro->getGeneros(),'Viajes')!== false ){print('checked'); }?> >VIAJES</span>
						<br>
					</div>


					Fecha Publicación:
					 <span class="error">
					 	<?php
					 		//SI exite el error 
							if(isset($error['errorFechaPubliUp'])){
								//Lo Mostramos
								print $error['errorFechaPubliUp'];
							}
						?>
					</span>
					<br> 
					<input type="text" name='fechaPubliUp' id="datepicker" value="<?php print($libro->getFecha_Publi()) ?>">
					<br>


			        Archivo Epub:
					<span class="error"> 
						<?php
					 		//SI exite el error 
							if(isset($error['errorArchivoUp'])){
							//Lo Mostramos
								print $error['errorArchivoUp'];
							}
						?>
					</span>
					<br>
					<input type="file" name="epubUp" /> 
			        <br>


					Sinopsis:
					<span class="error">
						<?php
					 		//SI exite el error 
							if(isset($error['errorSinopsisUp'])){
								//Lo Mostramos
								print $error['errorSinopsisUp'];
							}
						?>
					</span>
					<br> 
					<textarea name="sinopsisUp" id="sinopsisUp" cols="30" rows="15" ><?php print_r($libro->getSinopsis()) ?></textarea>
					<br>

        			<input type="submit" name="modificar" value="Modificar" />
					<input type="submit" name="atras" value="Atras" />
        	
				</form>
			</div>
		</div>
	</section>