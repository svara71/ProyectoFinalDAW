<header>
	
	<div id="inicio">
		<form method="post" action="index.php?location=inicio">
			<input type="submit" name="Inicio" value="" id="botonInicio">
		</form>	
	</div> 
	<div id="titulo"><h2>Libro</h2></div>
	<div id="sesion">
		<form method="post" action="index.php?location=inicio">
			<?php 
				if(isset($_SESSION['usuario'])){
					if($_SESSION['usuario']->getAdmin()==1){
			?>
						<input type="submit" name="Perfil" value="" id="botonPerfil">
						<input type="submit" name="Cerrar" value="" id="botonCerrar">
			<?php 
					}else{
			?>	
						<input type="submit" name="PaginaUser" value="" id="botonUser">
						<input type="submit" name="Cerrar" value="" id="botonCerrar">
			<?php 
					}
				}else{
			?>
				<input type="submit" name="login" value="" id="botonSesion">
			<?php  
				}
			?>
			

		</form>	
	</div>
</header>

<section id="pagInfolibro">
<div id="contenido">
	<div id="libro">
		<div id="portada">
			<img src="<?php print_r($_SESSION['buscarLibro']->getPortada()); ?>" alt="img">
		</div>
		<div id="caracLibro">
			<span><strong>TITULO:</strong><?php print_r($_SESSION['buscarLibro']->getNom_Libro()); ?></span>
			<span><strong>AUTOR:</strong> <?php print_r($_SESSION['buscarLibro']->getAutor()); ?> </span>
			<span><strong>GENEROS:</strong> <?php print_r($_SESSION['buscarLibro']->getGeneros()); ?></span>
			<span><strong>NUMERO PAGINAS: </strong><?php print_r($_SESSION['buscarLibro']->getNum_Pag()); ?></span>
			<span><strong>SINOPSIS:</strong> <?php print_r($_SESSION['buscarLibro']->getSinopsis()); ?>  </span>
		</div>
	</div>
		<div id="botonesInfoLibro">
					
				<form method="post" action="index.php?location=infolibro">
					<?php 

					//Si se a iniciado sesion
					if(isset($_SESSION['usuario'])){
						//Si libro compras es false
						if($_SESSION['usuario']->getAdmin()==1){
							//Se muestra ver y descargar
							?>
							<a class="leer" href="index.php?location=infolibro&ver&Referencia=<?php print_r($_SESSION['buscarLibro']->getRef_Libro()); ?>"><span>Leer</span></a>

							<a class="descargar" href="Descargar/epub/
							<?php
									 print_r($_SESSION['buscarLibro']->getNom_Libro().'.epub'); 
							 ?>"><span>Descargar</span></a>

							<a class="editar" href="index.php?location=infolibro&editar&Referencia=<?php print_r($_SESSION['buscarLibro']->getRef_Libro()); ?>"><span>Editar Libro</span></a>
							<?php 
							//si no
						}else if($libroComprado){
							//Se muestra ver y descargar
							?>
							<a class="leer" href="index.php?location=infolibro&ver&Referencia=<?php print_r($_SESSION['buscarLibro']->getRef_Libro()); ?>"><span>Ver</span></a>
							<a class="descargar" href="Descargar/epub/<?php print_r($_SESSION['buscarLibro']->getNom_Libro().'.epub') ?>"><span>Descargar</span></a>
							<?php 
							//si no
			
						}else{
								//Se muestra comprar
							?>
							<a href="index.php?location=infolibro&comprar&Referencia=<?php print_r($_SESSION['buscarLibro']->getRef_Libro()); ?>"><span>Comprar <?php print_r($_SESSION['buscarLibro']->getPrecio().'€'); ?></span></a>
							<a class="leerMuestra" href="index.php?location=infolibro&ver1cap&Referencia=<?php print_r($_SESSION['buscarLibro']->getRef_Libro()); ?>"><span>Ver Muestra Gratuita</span></a>
							<?php 
						}



					//Si no se a iniciado sesion 
					}else{
						//Se muestra inicio sesion
						?>
						<p>Si desea comprar un Libro inicia sesion o registrate
						<input type="submit" name="login" value="Inicio Sesion"></p>
						<a class="leerMuestra" href="index.php?location=infolibro&ver1cap&Referencia=<?php print_r($_SESSION['buscarLibro']->getRef_Libro()); ?>"><span>Ver Muestra Gratuita</span></a>
						<?php 
					}
					?>
				</form>	
		
	</div>
</div>		
</section>	


