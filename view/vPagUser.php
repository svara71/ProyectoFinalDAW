
<header>
	
	<div id="inicio">
		<form method="post" action="index.php?location=inicio">
			<input type="submit" name="Inicio" value="" id="botonInicio">
		</form>	
	</div> 
	<div id="titulo"><h2>Inicio Usuario</h2></div>
	<div id="sesion">
		<form method="post" action="index.php?location=inicio">
			<?php 
				if(isset($_SESSION['usuario'])){
					if($_SESSION['usuario']->getAdmin()==1){
			?>
						<input type="submit" name="Perfil" value="" id="botonPerfil">
						<input type="submit" name="Cerrar" value="" id="botonCerrar">
			<?php 
					}else{
			?>	
						<input type="submit" name="Perfil" value="" id="botonPerfil">
						<input type="submit" name="Cerrar" value="" id="botonCerrar">
			<?php 
					}
				}else{
			?>
				<input type="submit" name="login" value="" id="botonSesion">
			<?php  
				}
			?>
			

		</form>	
	</div>
</header>

<section id="pagUser">
	
	<nav>
		<ul>
			<a href="index.php?location=paguser&Genero&tipo=Autoayuda"><li>Autoayuda</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Aventura"><li>Aventura</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Biográfica"><li>Biográfica</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Ciencia Ficción"><li>Ciencia Ficción</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Cultura"><li>Cultura</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Deportes"><li>Deportes</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Drama"><li>Drama</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Economía "><li>Economía </li></a>
			<a href="index.php?location=paguser&Genero&tipo=Erótica"><li>Erótica</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Fantasía"><li>Fantasia</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Infantil"><li>Infantil</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Investigación"><li>Investigación</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Juvenil"><li>Juvenil</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Poesia"><li>Poesia</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Policial"><li>Policial</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Política"><li>Política</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Romántica"><li>Romántica</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Sociedad"><li>Sociedad</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Terror"><li>Terror</li></a>
			<a href="index.php?location=paguser&Genero&tipo=Viajes"><li>Viajes</li></a>
		</ul>
	</nav>
<div id="contenido">
			<div id="busqueda">
				<form  method="post" action="index.php?location=paguser">
					<input type="text" name="nom">
					<input type="submit" name="buscar" value="Buscar">
				</form> 
			</div>	
			<div id="infolibro">
		<?php 
			if(isset($_SESSION['usuario'])){
				//Si existe la variable de sesion buscar
				if(isset($_SESSION['buscarLibrosComprados'])){
						if(isset($_SESSION['nomCom'])){
							print('<h2 style="width:100%;margin:10px;">');
							print('LIBROS COMPRADOS QUE COINCIDEN CON "'.$_SESSION['nomCom'].'"');
							print('</h2>');
						}else if(isset($_SESSION['tipoCom'])){
							print('<h2 style="width:100%;margin:10px;">');
							print('LIBROS COMPRADOS DEL GENERO '.$_SESSION['tipoCom']);
							print('</h2>');
						}
						
					//Se recorre con un foreach
					foreach ($_SESSION['buscarLibrosComprados'] as $key) {
						print("<div>");
						 //Se muestran los libros y sus campos 
							print("<div class='img'>");
								?>
									<img src="<?php print_r($key['Portada']); ?>" alt="img"> 
									 
								<?php  
							print("</div>");
							print("<div class='info'>");
								print("<span class='titulolibro'>");
									print('<strong>');
									print('TITULO: ');
									print('</strong>');
									print_r($key['Nom_Libro']);
								print("</span>");
								print("<span class='autor'>");
									print('<strong>');
									print('AUTOR: ');
									print('</strong>');
									print_r($key['Autor']);
								print("</span>");
								print("<span class='sinopsis' >");
									print('<strong>');
									print('SINOPSIS: ');
									print('</strong>');
									print_r($key['Sinopsis']);
								print("</span>");
								?>
									<a href="index.php?location=paguser&InfoLibro&Referencia=<?php print_r($key['Ref_Libro']); ?>">Mas...</a>
								<?php

							print("</div>");
						
						print("</div>"); 	
					}
					
				}
				else{
					//Si existe la variable de sesion lisar
					if(isset($_SESSION['listaLibrosComprados'])){
						//se recorre
							print('<h2 style="width:100%;margin:10px;">');
							print('LIBROS COMPRADOS');
							print('</h2>');
						foreach ($_SESSION['listaLibrosComprados'] as $key) {
						print("<div>");
						 //Se muestran los libros y sus campos 
							print("<div class='img'>");
								?>
									<img src="<?php print_r($key['Portada']); ?>" alt="img"> 
									 
								<?php  
							print("</div>");
							print("<div class='info'>");
								print("<span class='titulolibro'>");
									print('<strong>');
									print('TITULO: ');
									print('</strong>');
									print_r($key['Nom_Libro']);
								print("</span>");
								print("<span class='autor'>");
									print('<strong>');
									print('AUTOR: ');
									print('</strong>');
									print_r($key['Autor']);
								print("</span>");
								print("<span class='sinopsis' >");
									print('<strong>');
									print('SINOPSIS: ');
									print('</strong>');
									print_r($key['Sinopsis']);
								print("</span>");
								?>
									<a href="index.php?location=paguser&InfoLibro&Referencia=<?php print_r($key['Ref_Libro']); ?>">Mas...</a>
								<?php

							print("</div>");
						
						print("</div>"); 	
					}
				}
			}
		}
			?>
	</div>		
	</div>	
</section>
