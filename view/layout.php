<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8"/>
<link rel="icon" type="image/png" href="webroot/css/image/libro.png" />
<title></title>
	<?php if(isset($_SESSION['directorio'])){
		?>
		<link rel="stylesheet" href="<?php print_r($_SESSION['directorio'].'/OEBPS/Styles/style.css') ?>">
		<?php 
	} ?>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="./webroot/css/styles.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript">
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'yy-mm-dd',
		changeYear: true,
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearRange: "1897:2017",
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
 
	$(document).ready(function(){
		$("#datepicker").datepicker({
		changeMonth: true
    });
});
</script>
</head>
<body>

			<?php 
			
				/**
				 * 
				 * Fichero layout.php
				 * 
				 * 
				 * Fichero que contiene la apariencia de la aplicación
				 * 
				 * 
				 * @author Samuel Vara García
				 * 
				 * @package /view
				 * 
				 * 
				 */

				
					$layout ="view/vInicio.php";
					 //Se comprueba que hay se haya creado una header(location: ) y que hay una localizacion del la vista igual en el array de controladores de config.php
					if(isset($_GET['location']) && isset($vistas[$_GET['location']])){
						//si es asi se la pasa a un avariable
						$layout=$vistas[$_GET['location']];
					}
					//Si no, se muestra el layout (Que mostrará la pagina del valor a la que referencie) 
					include $layout;

				?>	

				
</body>
<footer >
	<a href="https://gitlab.com/svara71/ProyectoFinalDAW" ><img	src="./webroot/image/github.png"></a>
</footer>
</html>