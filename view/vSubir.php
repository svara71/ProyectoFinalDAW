
<header>
	
	<div id="inicio">
		<form method="post" action="index.php?location=inicio">
			<input type="submit" name="Inicio" value="" id="botonInicio">
		</form>	
	</div> 
	<div id="titulo"><h2>Inicio Sesión</h2></div>
	<div id="sesion">
		<form method="post" action="index.php?location=inicio">
			<?php 
				if(isset($_SESSION['usuario'])){
					if($_SESSION['usuario']->getAdmin()==1){
			?>
						<input type="submit" name="Perfil" value="" id="botonPerfil">
						<input type="submit" name="Cerrar" value="" id="botonCerrar">
			<?php 
					}else{
			?>	
						<input type="submit" name="PaginaUser" value="" id="botonUser">
						<input type="submit" name="Cerrar" value="" id="botonCerrar">
			<?php 
					}
				}else{
			?>
				<input type="submit" name="login" value="" id="botonSesion">
			<?php  
				}
			?>
			

		</form>	
	</div>
</header>

<section id="subir">
		<div id="contenido">
			<div id="formSubir">
				<form method="post" action="index.php?location=subir" enctype="multipart/form-data">
						<h2>Subir Libro</h2>

					Nombre del Libro:
					<span class='error'>
						<?php  
						//SI exite el error 
							if(isset($error['errorNomLibroNew'])){
								//Lo Mostramos
								print $error['errorNomLibroNew'];
							}
						?>
					</span>
					 <br>
					 <input type="text" name="nomLibro">
					 <br>

					 ISBN:
					<span class='error'>
						<?php  
						//SI exite el error 
							if(isset($error['errorISBNNew'])){
								//Lo Mostramos
								print $error['errorISBNNew'];
							}
						?>
					</span>
					<br>
					<input type="text" name="isbn">
					<br>

					 Autor:
					<span class="error">
						<?php  
							//SI exite el error 
							if(isset($error['errorAutorNew'])){
								//Lo Mostramos
								print $error['errorAutorNew'];
							}
						?>
					</span>
					<br>		
					<input type="text" name="autor">
				    <br>

					Numero de Paginas:
					<span class="error">
						<?php  
							//SI exite el error 
							if(isset($error['errorNumPagNew'])){
								//Lo Mostramos
								print $error['errorNumPagNew'];
							}
						?>
					</span>
					<br> 
					<input type="text" name="numPag">
					<br>


					Precio:
					<span class="error">
						<?php  
						//SI exite el error 
							if(isset($error['errorPrecioNew'])){
								//Lo Mostramos
								print $error['errorPrecioNew'];
							}
						?>
					</span>
					<br>  
					<input type="text" name="precio">
					<br>
					<br> 

					Generos:
					 <span class="error">
					 	<?php
					 		//SI exite el error 
							if(isset($error['errorGenerosNew'])){
								//Lo Mostramos
								print $error['errorGenerosNew'];
							}
						?>
					</span>
					 <br>
					<div id="generos">
					 <br>
						<span><input type="checkbox" name="generos[]" value="Autoayuda">AUTOAYUDA</span>
						<span><input type="checkbox" name="generos[]" value="Aventura">AVENTURA</span>
						<span><input type="checkbox" name="generos[]" value="Biográfica">BIOGRÁFICA</span>
						<span><input type="checkbox" name="generos[]" value="Ciencia Ficción">CIENCIA FICCIÓN</span>
						<span><input type="checkbox" name="generos[]" value="Cultura">CULTURA</span>
						<span><input type="checkbox" name="generos[]" value="Deportes">DEPORTES</span>
						<span><input type="checkbox" name="generos[]" value="Drama">DRAMA</span>
						<span><input type="checkbox" name="generos[]" value="Economía">ECONOMÍA</span>
						<span><input type="checkbox" name="generos[]" value="Erótica">ERÓTICA</span>
						<span><input type="checkbox" name="generos[]" value="Fantasía">FANTASIA</span>
						<span><input type="checkbox" name="generos[]" value="Infantil">INFANTIL</span>
						<span><input type="checkbox" name="generos[]" value="Investigación">INVESTIGACIÓN</span>
						<span><input type="checkbox" name="generos[]" value="Juvenil">JUVENIL</span>
						<span><input type="checkbox" name="generos[]" value="Poesia">POESIA</span>
						<span><input type="checkbox" name="generos[]" value="Policial">POLICIAL</span>
						<span><input type="checkbox" name="generos[]" value="Política">POLÍTICA</span>
						<span><input type="checkbox" name="generos[]" value="Romántica">ROMÁNTICA</span>
						<span><input type="checkbox" name="generos[]" value="Sociedad">SOCIEDAD</span>
						<span><input type="checkbox" name="generos[]" value="Terror">TERROR</span>
						<span><input type="checkbox" name="generos[]" value="Viajes">VIAJES</span>
						<br>
					</div>


					Fecha Publicación:
					 <span class="error">
					 	<?php
					 		//SI exite el error 
							if(isset($error['errorFechaPubliNew'])){
								//Lo Mostramos
								print $error['errorFechaPubliNew'];
							}
						?>
					</span>
					<br> 
					<input type="text" name='fechaPubli' id="datepicker">
					<br>


			        Archivo Epub:
					<span class="error"> 
						<?php
					 		//SI exite el error 
							if(isset($error['errorArchivoNew'])){
							//Lo Mostramos
								print $error['errorArchivoNew'];
							}
						?>
					</span>
					<br>
					<input type="file" name="epub" /> 
			        <br>


					Sinopsis:
					<span class="error">
						<?php
					 		//SI exite el error 
							if(isset($error['errorSinopsisNew'])){
								//Lo Mostramos
								print $error['errorSinopsisNew'];
							}
						?>
					</span>
					<br> 
					<textarea name="sinopsis" id="sinopsis" cols="30" rows="15"></textarea>
					<br>

        			<input type="submit" name="enviar" value="Enviar" />
				</form>
			</div>
		</div>
	</section>