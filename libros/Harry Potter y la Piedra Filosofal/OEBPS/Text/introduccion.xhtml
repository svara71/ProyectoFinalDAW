<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title></title>
  <link href="../Styles/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <h4 id="heading_id_2">Introducción</h4>

  <h5 class="izquierda sinmargen saltito" id="heading_id_3">Sobre este libro</h5>

  <p>Animales fantásticos y dónde encontrarlos representa el fruto de muchos años de viajes e investigaciones. Al echar la vista atrás, recuerdo al mago de siete años que pasaba horas en su dormitorio despedazando horklumps y envidio los viajes que habría de realizar: desde la jungla más oscura hasta el desierto más deslumbrante, desde el pico de las montañas hasta las ciénagas. Al crecer, ese chico mugriento recubierto con restos de horklump perseguiría a las bestias que se describen en las páginas siguientes. He visitado madrigueras, guaridas y nidos en cientos de regiones, he sido testigo de sus poderes, me he ganado su confianza y, en ocasiones, he rechazado sus ataques con mi tetera de viaje.</p>

  <p>La primera edición de Animales fantásticos… nació de un encargo que me hizo en 1918 el señor Augustus Worme de Obscurus Books, quien tuvo la gentileza de preguntarme qué me parecería la idea de escribir un compendio autoritativo de criaturas mágicas para su editorial.</p>

  <p>Entonces yo no era más que un empleado de bajo rango del Ministerio de Magia y acepté de inmediato una oportunidad que me permitía tanto complementar mi magro salario de dos sickles por semana como pasar las vacaciones viajando por el mundo para buscar nuevas especies mágicas. El resto ya es historia de la industria editorial: Animales fantásticos… lleva cincuenta y dos ediciones.</p>

  <p class="salto">El objetivo de esta introducción es contestar a algunas de las preguntas más frecuentes que han ido llegando en sacas de correo todas las semanas desde que este libro se imprimiera por primera vez en 1927. La primera pregunta que trataremos de responder es la fundamental: ¿qué es una «bestia»?</p>

  <p><img alt="" src="../Images/bestia.jpg" /></p>

  <h5 class="izquierda sinmargen saltito" id="heading_id_4">¿Qué es una bestia?</h5>

  <p>La definición de «bestia» ha sido objeto de controversia durante siglos. Aunque este hecho pueda sorprender a algunos estudiantes que se acerquen por primera vez a la Magizoología, el problema puede resultar más comprensible si nos tomamos un momento para considerar tres tipos de criaturas mágicas.</p>

  <p>Los hombres lobo son humanos la mayor parte del tiempo (ya sean magos o muggles). Sin embargo, una vez al mes, se transforman en bestias salvajes de cuatro patas con intenciones asesinas y sin ninguna conciencia humana.</p>

  <p>Los hábitos de los centauros no son como los de los humanos, pues viven en estado natural, se niegan a vestirse y rehuyen tanto a magos como a muggles, aunque tienen una inteligencia igual a la de ellos.</p>

  <p>Los trolls presentan una apariencia remotamente humana, caminan erguidos y pueden aprender algunas palabras simples, pero son menos inteligentes que el más tonto de los unicornios y no tienen poderes mágicos si exceptuamos su prodigiosa y anormal fuerza.</p>

  <p>Ahora preguntémonos: ¿cuál de esas criaturas es un «ser» -es decir, una criatura digna de derechos legales y voz en el gobierno del mundo mágico- y cuál una «bestia»?</p>

  <p>Las primeras tentativas para decidir qué criaturas mágicas debían ser consideradas «bestias» fueron sumamente rudimentarias.</p>

  <p>Burdock Muldoon, presidente del Consejo de Magos<sup><a href="../Text/notas.xhtml#nota1" id="nota1">[1]</a></sup> en el siglo XIV, decretó que en adelante cualquier miembro de la comunidad mágica que caminara sobre dos piernas sería considerado un «ser», mientras que los demás seguirían siendo «bestias». En un gesto amistoso, convocó a todos los «seres» para que se reunieran con los magos en una cumbre que discutiría nuevas leyes mágicas y descubrió, para su enorme desaliento, que se había equivocado. La sala de reuniones estaba repleta de duendes que habían llevado a todas las criaturas con dos piernas que habían podido encontrar. Como nos cuenta Bathilda Bagshot en su libro Una historia de la magia:</p>

  <p class="cita cursiva asangre">Apenas se oía nada entre los graznidos de los diricawls, los lamentos de los augureys y el canto incesante y taladrador de los fwoopers. Cuando las brujas y los magos intentaron consultar los documentos ante ellos, un grupo de hadas y duendecillos empezó a dar vueltas entre sus cabezas, riendo y parloteando. Un grupo de trolls comenzó a destrozar las paredes del recinto con sus mazas, mientras unas arpías se deslizaban por el lugar buscando niños para comérselos. El presidente del Consejo se puso en pie para abrir la sesión, resbaló en una pila de excrementos de porlock y abandonó apresuradamente la sala sin dejar de maldecir.</p>

  <p>Como podemos ver, la simple posesión de dos piernas no garantizaba que una criatura mágica pudiera o quisiera interesarse por los asuntos del gobierno de los magos. Amargado, Burdock Muldoon renunció a cualquier intento de integrar en el Consejo a otros miembros de la comunidad mágica que no fueran magos.</p>

  <p>La sucesora de Muldoon, la señora Elfrida Clagg, intentó redefinir el concepto de «seres» con la esperanza de estrechar lazos con otras criaturas mágicas. Los «seres», declaró, son aquellos que pueden hablar el lenguaje humano. Por lo tanto, todos aquellos que pudieran hacerse entender por los miembros del Consejo estaban invitados a participar en la siguiente reunión. No obstante, una vez más hubo problemas. Los duendes enseñaron algunas frases sencillas a los trolls y éstos volvieron a destrozar la sala. Los jarveys se dedicaron a correr entre las patas de las sillas y mordieron todos los tobillos que estaban a su alcance. Entretanto, había llegado una numerosa delegación de fantasmas (que habían sido apartados bajo la presidencia de Muldoon con el argumento de que no andaban, sino que se deslizaban), pero se retiraron indignados por lo que luego calificaron como «desvergonzado énfasis que el Consejo ponía en las necesidades de los vivos, en oposición a los deseos de los muertos». Los centauros, que con Muldoon habían sido clasificados como «bestias» y eran ahora definidos como «seres» bajo la presidencia de la señora Clagg, se negaron a asistir al Consejo en protesta por la exclusión de la gente del agua, que sólo podía hablar en sirenio.</p>

  <p>Hubo que esperar hasta 1811 para que se diera con definiciones que la mayoría de la comunidad mágica considerara aceptables. Grogan Stump, el por entonces recién nombrado ministro de Magia, decretó que un «ser» era «cualquier criatura que tenga suficiente inteligencia para comprender las leyes de la comunidad mágica y compartir parte de la responsabilidad que implica su formulación».<sup><a href="../Text/notas.xhtml#nota2" id="nota2">[2]</a></sup> Los representantes de los trolls fueron entrevistados en ausencia de duendes y se concluyó que no comprendían nada de lo que se les estaba diciendo; por lo tanto, los clasificaron como «bestias», aunque caminaban sobre dos piernas. La gente del agua fue invitada a través de traductores a asumir el estatuto de «seres» por primera vez. Pese a su apariencia humanoide, hadas, gnomos y duendecillos fueron catalogados como «bestias» con total contundencia.</p>

  <p>Naturalmente, el asunto no terminó ahí. Todos conocemos a los extremistas que hacen campaña para clasificar a los muggles como «bestias»; todos sabemos que los centauros han rehusado el estatuto de «seres» y han solicitado que los sigan considerando «bestias»;<sup><a href="../Text/notas.xhtml#nota3" id="nota3">[3]</a></sup> mientras tanto, los hombres lobo han ido pasando de la División de Bestias a la de Seres y viceversa durante muchos años; en el momento en que escribimos esto, hay una Oficina de Servicio de Apoyo para Hombres Lobo en la División de Seres, mientras que el Registro de Hombres Lobo y la Unidad de Captura de Hombres Lobo entran en la División de Bestias. Muchas criaturas sumamente inteligentes son clasificadas como «bestias» porque no logran superar sus brutales instintos. Las acromántulas y las mantícoras son capaces de mantener una conversación racional, pero intentarán devorar a cualquier humano que se les acerque. Las esfinges hablan solamente con enigmas y acertijos y son violentas cuando les dan una respuesta equivocada.</p>

  <p>En las páginas siguientes he señalado los casos donde perdura la incertidumbre sobre la clasificación de una bestia.</p>

  <p class="salto">Vamos ahora a planteamos lo que magos y brujas se preguntan más a menudo cuando la charla versa sobre criaturas mágicas: ¿por qué?</p>

  <p class="indentado"><img alt="" src="../Images/mentiroso.jpg" /></p>

  <h5 class="izquierda sinmargen saltito" id="heading_id_5">Una breve historia del conocimiento muggle de las criaturas fantásticas</h5>

  <p>Por más asombroso que pueda resultarle a la mayoría de los magos, los muggles no siempre ignoraron la existencia de esas criaturas monstruosas y mágicas que nosotros hemos tratado de ocultar durante tanto tiempo y con tanto ahínco. Una mirada al arte y la literatura muggles de la Edad Media revela que gran parte de las criaturas que ahora se consideran imaginarias, se consideraban entonces reales. El dragón, el grifo, el unicornio, el fénix, el centauro: ellos y muchos otros están representados en las obras muggles de ese período, aunque habitualmente con inexactitudes casi cómicas.</p>

  <p>Sin embargo, un examen más cuidadoso de los bestiarios muggles de esa época demuestra que la mayoría de las criaturas mágicas burlaron totalmente la atención de los muggles o fueron confundidas con otras cosas. Examinen este fragmento de manuscrito que ha llegado hasta nuestros días y que escribió el hermano Benedicto, un monje franciscano de Worcestershire:</p>

  <p class="cita cursiva asangre">Hoy, mientras trabajaba en el jardín de plantas medicinales, aparté la albahaca para descubrir un hurón de tamaño descomunal. Ni corrió ni se escondió como hacen los hurones, sino que se arrojó sobre mí y me tiró de espaldas sobre la tierra mientras gritaba con una furia anormal: «¡Fuera de aquí, pelón!" y entonces me mordió la nariz con tanta maldad que me sangró durante varias horas. El fraile no estaba dispuesto a creer que me había encontrado con un hurón que hablaba y me preguntó si había estado bebiendo del vino del hermano Bonifacio. Como la nariz estaba hinchada y me sangraba, me excusó de las vísperas.</p>

  <p>Es evidente que nuestro amigo muggle no había desenterrado un hurón, como suponía, sino a un jarvey, que seguramente estaba persiguiendo a su presa favorita, los gnomos.</p>

  <p>La comprensión incorrecta es a menudo más peligrosa que la ignorancia, y el miedo que los muggles tenían a la magia aumentó indudablemente por el temor que les inspiraba lo que pudiera estar al acecho en sus huertas. En esa época la persecución de magos estaba alcanzando un punto hasta entonces desconocido, y divisar criaturas tales como hipogrifos y dragones contribuía a acrecentar la histeria muggle.</p>

  <p>No es propósito de esta obra discutir la oscura etapa que llevó a los magos a esconderse.<sup><a href="../Text/notas.xhtml#nota4" id="nota4">[4]</a></sup> Lo que nos interesa conocer ahora es el destino de esas bestias fabulosas que, al igual que nosotros, tenían que ser escondidas para conseguir que los muggles se convencieran de que la magia no existía.</p>

  <p>La Confederación Internacional de Magos planteó el asunto en la famosa cumbre de 1692. Hechiceros de todas las nacionalidades dedicaron por lo menos siete semanas de discusiones a veces ásperas a la problemática cuestión de las criaturas mágicas. ¿Cuántas especies podríamos ocultar para que no fueran detectadas por los muggles y cuáles debíamos elegir? ¿Dónde y cómo debíamos esconderlas? La polémica se recrudeció: algunas criaturas no parecían conscientes de que era su propio destino el que se estaba decidiendo; otras contribuían activamente al debate.<sup><a href="../Text/notas.xhtml#nota5" id="nota5">[5]</a></sup> Finalmente se llegó a un acuerdo.<sup><a href="../Text/notas.xhtml#nota6" id="nota6">[6]</a></sup> Veintisiete especies, cuyo tamaño abarcaba desde dragones hasta bundimuns, debían ser escondidas de los muggles para crear la ilusión de que nunca habían existido más que en su imaginación.</p>

  <p>Esa cantidad se incrementó durante el siglo siguiente, a medida que los magos adquirieron más confianza en sus métodos de ocultamiento.</p>

  <p>En 1750, se insertó la cláusula número 73 en el Estatuto Internacional del Secreto de los Brujos, a la que se ajustan los Ministerios de Magia del mundo entero:</p>

  <blockquote class="cita cursiva">
    <p class="asangre">Cada organismo de gobierno mágico será responsable del ocultamiento, cuidado y control de todas las bestias, seres y espíritus de naturaleza mágica que habiten en los límites de su territorio.</p>

    <p class="asangre">Si cualquiera de esas criaturas causara daño a la comunidad muggle o se mostrara ante ella, el organismo mágico gubernamental de esa nación deberá someterse a las sanciones que decida la Confederación Internacional de Magos.</p>
  </blockquote>

  <h5 class="izquierda sinmargen saltito" id="heading_id_6">El ocultamiento de las criaturas mágicas</h5>

  <p>Sería absurdo negar que se han producido infracciones esporádicas de la cláusula número 73 desde que entró en vigor por primera vez. Los lectores ingleses de más edad recordarán el «episodio de Ilfracombe» sucedido en 1932, cuando un pícaro dragón galés verde común se abalanzó sobre una playa abarrotada de muggles en bañador. Por fortuna, la valiente actitud de una familia de magos que estaba de vacaciones impidió que se produjeran desgracias. Por esa razón se la premiaría con la Orden de Merlín, Primera Clase. Aquella familia realizó la mayor tanda de encantamientos desmemorizantes de este siglo sobre los habitantes de Ilfracombre, con lo que evitó la catástrofe por los pelos.<sup><a href="../Text/notas.xhtml#nota7" id="nota7">[7]</a></sup></p>

  <p>La Confederación Internacional de Magos ha tenido que multar a ciertas naciones repetidas veces por contravenir la cláusula número 73. TIoet y Escocia son dos de las infractoras más persistentes. Los muggles han visto en tantas ocasiones al yeti, que la Confederación Internacional de Magos se vio obligada a instalar una Fuerza Operativa Internacional en las montañas con carácter permanente. Entretanto, el kelpie más grande del mundo continúa evadiendo su captura en el lago Ness y parece haber desarrollado una verdadera sed de publicidad.</p>

  <p>A pesar de esos desafortunados percances, los magos podemos enorgullecemos de un trabajo bien hecho. No hay duda de que la inmensa mayoría de los muggles de hoy se niegan a creer en esas criaturas mágicas que sus antepasados temieron tanto. Incluso los muggles que han visto excrementos de porlock o huellas de streeler -pues sería una tontería suponer que todos los rastros de las bestias pueden ocultarse- se dan por satisfechos con cualquier explicación que no esté relacionada con la magia, por extraña que sea.<sup><a href="../Text/notas.xhtml#nota8" id="nota8">[8]</a></sup> Si un muggle es lo bastante imprudente para confiarle a otro que ha divisado un hipogrifo que volaba hacia el norte, lo más normal es que el otro piense que está borracho o que ha perdido la chaveta. Por injusto que resulte para el muggle en cuestión, no deja de ser preferible a que te quemen en la hoguera o te ahoguen en el estanque del pueblo.</p>

  <p>Pero ¿cómo consigue la comunidad mágica ocultar a las bestias fantásticas?</p>

  <p>Por suerte, algunas especies no requieren mucha ayuda de los magos para evitar que los muggles las detecten. Criaturas tales como el tebo, el demiguise y el bowtruckle tienen métodos de camuflaje altamente efectivos, por lo que nunca ha sido necesaria la intervención del Ministerio de Magia para defenderlas. Luego están esas criaturas que, debido a su inteligencia o a una timidez innata, evitan el contacto con los muggles a toda costa, como, por ejemplo, el unicornio, el mooncalfy, el centauro. Otras bestias mágicas habitan en lugares inaccesibles para los muggles: enseguida nos viene a la cabeza la acromántula, que vive en lo más recóndito del territorio inexplorado de la jungla de Borneo, y el fénix, que anida en los picos de montañas que son inalcanzables sin el uso de la magia.</p>

  <p>Por último, los casos más frecuentes son los de criaturas lo bastante pequeñas, rápidas o hábiles para hacerse pasar por animales corrientes y no reclamar la atención de los muggles; los chizpurfles, billywigs y crups entran en esa categoría.</p>

  <p class="salto">De todos modos, también hay muchas bestias que, sea o no de manera intencionada, siguen siendo llamativas incluso a ojos de un muggle, y éstas son las que generan una notable cantidad de trabajo al Departamento de Regulación y Control de las Criaturas Mágicas. Este departamento, el segundo con la plantilla más grande del Ministerio de Magia,<sup><a href="../Text/notas.xhtml#nota9" id="nota9">[9]</a></sup> se ocupa de las diversas necesidades de la multitud de especies que están a su cuidado y las atiende de diferentes maneras.</p>

  <h5 class="izquierda sinmargen saltito" id="heading_id_7">Hábitats seguros</h5>

  <p>Tal vez la medida más importante para ocultar a las criaturas mágicas fue la creación de hábitats seguros. Los encantamientos repelentes de muggles impiden que haya intrusos tanto en los bosques donde viven unicornios y centauros como en los lagos y ríos reservados para la gente del agua. En casos extremos, tales como el de los quintapeds, se han encantado parajes enteros para que sea imposible delimitarlos.<sup><a href="../Text/notas.xhtml#nota10" id="nota10">[10]</a></sup></p>

  <p class="salto">Algunas de esas zonas seguras deben mantenerse bajo constante supervisión mágica, por ejemplo, las reservas de dragones. Mientras que los unicornios y la gente del agua están encantados de quedarse en los territorios que les han asignado, los dragones aprovechan cualquier oportunidad para buscar una presa más allá de los límites de la reserva. En ciertos casos, los encantamientos repelentes de muggles no funcionan, ya que los propios poderes de las criaturas pueden anularlos. Ejemplos pertinentes son el kelpie, cuya única meta en la vida es atraer a personas, y el pogrebin, que busca a sus presas entre los seres humanos.</p>

  <h5 class="izquierda sinmargen saltito" id="heading_id_8">Control de cría y venta</h5>

  <p>La posibilidad de que un muggle se alarme ante la presencia de cualquiera de las criaturas mágicas más grandes o peligrosas se ha reducido notablemente gracias a las elevadas multas con que se castiga la cría y venta de huevos y criaturas jóvenes. El Departamento de Regulación y Control de las Criaturas Mágicas mantiene una estricta vigilancia sobre el comercio de bestias fantásticas. La Prohibición de la Reproducción Experimental de 1965 convirtió en ilegal la creación de nuevas especies.</p>

  <p class="asangre"><img alt="" src="../Images/hagrid.jpg" /></p>

  <h5 class="izquierda sinmargen saltito" id="heading_id_9">Embrujos desilusionadores</h5>

  <p class="salto">El mago de la calle también desempeña un papel importante en el ocultamiento de las bestias mágicas. Por ejemplo, aquellos que poseen un hipogrifo están obligados por ley a encantarlo con un embrujo desilusionador para distorsionar la visión de cualquier muggle que pudiera verlo. Los embrujos desilusionadores deben renovarse a diario, ya que sus efectos se disipan.</p>

  <h5 class="izquierda sinmargen saltito" id="heading_id_10">Encantamiento desmemorizante</h5>

  <p class="salto">Cuando sucede lo peor y un muggle ve lo que no debería haber visto, el encantamiento desmemorizante es tal vez el instrumento más útil para reparar el daño. El dueño del animal en cuestión puede realizar el encantamiento, pero, en casos muy graves de alerta muggle, el Ministerio de Magia puede enviar un equipo de desmemorizadores entrenados.</p>

  <h5 class="izquierda sinmargen saltito" id="heading_id_11">La Oficina de Desinformación</h5>

  <p>La Oficina de Desinformación se involucrará únicamente en situaciones extraordinarias de colisión entre el mundo mágico y el muggle. Algunas catástrofes o accidentes mágicos son demasiado notorios para que los muggles se los expliquen sin la ayuda de una autoridad exterior. En esos casos, la Oficina de Desinformación se pondrá en contacto directamente con el primer ministro muggle para buscar una explicación del suceso que sea plausible y que no tenga nada que ver con la magia. Gracias a que esta oficina no escatimó esfuerzos para persuadir a los muggles de que todas las evidencias fotográficas del kelpie del lago Ness son falsas, se ha conseguido salvar una situación que en cierto momento pareció demasiado peligrosa.</p>
</body>
</html>
