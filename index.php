


<?php 

error_reporting(E_ALL);
ini_set('display_errors', 'On');


header("Content-Type: text/html;charset=utf-8");
/**
 * 
 * Fichero Index.php
 * 
 * 
 * Controlador principar de la aplicacion 
 * 
 * 
 * @author Samuel Vara García
 * 
 * @package LibroWeb/
 * 
 * 
 */
	 
require_once 'model/Usuario.php';
require_once 'model/Libro.php';
//Recupera la session
session_start();
require_once 'config/config.php';
$controlador = 'controller/cInicio.php';
//Array de errores
$error=['errorIDuser' => '', 'errorPass' => '' ,'errorIDuserNew' => '', 'errorPassNew' => '' , 'errorNomNew' => '', 'errorApellNew' => '','errorFechaNacNew' => '' , 'errorEmailNew' => '','errorUserExist' => '','errorNoUser' => '', 'errorPassUp' => '' , 'errorNomUp' => '', 'errorApellUp' => '','errorFechaNacUp' => '' , 'errorEmailUp' => '','errorUserNoMod'=> '','errorPassUpConf'=>'','errorNomLibroNew'=>'','errorISBNNew'=>'','errorAutorNew'=>'','errorNumPagNew'=>'','errorPrecioNew'=>'','errorGenerosNew'=>'','errorFechaPubliNew'=>'','errorArchivoNew'=>'','errorSinopsisNew'=>'','errorNomLibroUp'=>'','errorISBNUp'=>'','errorAutorUp'=>'','errorNumPagUp'=>'','errorPrecioUp'=>'','errorGenerosUp'=>'','errorFechaPubliUp'=>'','errorArchivoUp'=>'','errorSinopsisUp'=>''];



 //Se comprueba que hay se haya creado una header(location: ) y que hay una localizacion del controlador igual en el array de controladores de config.php
if (isset($_GET['location']) && isset($controladores[$_GET['location']])) {
 	//Se le pasa la localizacion 
    $controlador = $controladores[$_GET['location']];
}

//Se muestra el valor del controlador (Que mostrará la pagina a la cual haga referencia login o inicio) 
include $controlador;

?>