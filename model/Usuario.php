<?php 
require_once 'UsuarioPDO.php';
/**
 * 
 * Clase Usuario
 * 
 * Clase que almacena los atributos del Usuario
 * 
 *  @author Samuel Vara García
 * 
 * 	@version 1.0.0
 * 
 */
class Usuario{

	//Atributo de la Clase Usuario
	protected $IDUser;
	//Atributo de la Clase Usuario
	protected $Pass;
	//Atributo de la Clase Usuario
	protected $Nom;
	//Atributo de la Clase Usuario
	protected $Apell;
	//Atributo de la Clase Usuario
	protected $Fecha_Nac;
	//Atributo de la Clase Usuario
	protected $Email;
	//Atributo de la Clase Usuario
	protected $Activo;
	//Atributo de la Clase Usuario
	protected $Admin;


	/**
	*
	* Función getIDUser
	*
	* Devuelve el valor de la ID de usuario del Usuario
	*
	*/
	public function getIDUser(){
		return $this -> IDUser;
	}
	
	/**
	*
	* Función getPass
	*
	* Devuelve el valor de la Contraseña del Usuario
	*
	*/
	public function getPass(){
		return $this -> Pass;
	}
	
	/**
	*
	* Función getNom
	*
	* Devuelve el valor de la Nombre del Usuario
	*
	*/
	public function getNom(){
		return $this -> Nom;
	}
	
	/**
	*
	* Función getApell
	*
	* Devuelve el valor de la Apellidos del Usuario
	*
	*/
	public function getApell(){
		return $this -> Apell;
	}
	
	/**
	*
	* Función getFechaNac
	*
	* Devuelve el valor de la Fecha Nacimiento del Usuario
	*
	*/
	public function getFechaNac(){
		return $this -> Fecha_Nac;
	}
	
	/**
	*
	* Función getEmail
	*
	* Devuelve el valor de la Email del Usuario
	*
	*/
	public function getEmail(){
		return $this -> Email;
	}
	
	/**
	*
	* Función getActivo
	*
	* Devuelve el valor de Activo del Usuario
	*
	*/
	public function getActivo(){
		return $this -> Activo;
	}
	/**
	*
	* Función getAdmin
	*
	* Devuelve el valor de Admin del Usuario
	*
	*/
	public function getAdmin(){
		return $this -> Admin;
	}
	
	/**
	*
	* Función __construct
	*
	* Constructor de la clase Usuario
	*
	*/
	public function __construct($IDUser,$Pass,$Nom,$Apell,$Fecha_Nac,$Email,$Activo,$Admin){
		$this -> IDUser = $IDUser;
		$this -> Pass = $Pass;
		$this -> Nom = $Nom;
		$this -> Apell = $Apell;
		$this -> Fecha_Nac = $Fecha_Nac;
		$this -> Email = $Email;
		$this -> Activo = $Activo;
		$this -> Admin = $Admin;
	}


	/**
	*
	*
	*Función validarUsuario
	*
	*Función que valida si un usuario existe en la base de datos
	*
	*@param string $IDUser
	*@param string $pass
	*
	*
	*@return objeto usuario $objUser
	*
	**/
	
	public static function validarUsuario($IDUser,$Pass){
		//Declaramos una variable a null
		$objUser=null;
		//Recogemos los datos Recibidos De la clase usuarioPDO en un array
		$arrayUser=UsuarioPDO::validarUsuario($IDUser,$Pass);

		//SI el array no esta vacio
		if($arrayUser){
			//Creamos un nuevo objeto Usuario y lo metemos en la variable ObjUser
			$objUser = new Usuario( $arrayUser['IDUser'] ,$arrayUser['Pass'],$arrayUser['Nom'] ,$arrayUser['Apell'],$arrayUser['Fecha_Nac'],$arrayUser['Email'],$arrayUser['Activo'],$arrayUser['Admin']);
		}
		//La devolvemos 
		return $objUser;
	}

	/**
	*
	* Función insertarUsuario
	*
	*
	* Función que añade un nuevo usuario a la base de datos
	*
	* @param string $IDUser
	* @param string $Pass
	* @param string $Nom
	* @param string $Apell
	* @param string $Fecha_Nac
	* @param string $Email
	*
	*
	* @return boolean $usuarioInsert
	*/
	public static function insertarUsuario($IDUser,$Pass,$Nom,$Apell,$Fecha_Nac,$Email){
		//Recogemos la respuesta Recibida De la clase usuarioPDO
		$insert=UsuarioPDO::insertarUsuario($IDUser,$Pass,$Nom,$Apell,$Fecha_Nac,$Email);
		
		//la devolvemos
		return $insert;
	}


	/**
	*
	* Función activoDesactivo
	*
	*
	* Función que activa o desactiva la cuenta
	*
	* @param string $IDUser
	* @param string $Activo
	*
	*
	* @return boolean $update
	*/
	public static function activoDesactivo($Activo,$IDUser){
		//Recogemos la respuesta Recibida De la clase usuarioPDO
		$update=UsuarioPDO::activoDesactivo($Activo,$IDUser);
		
		//la devolvemos
		return $update;
	}

	/**
	*
	* Función borrarUsuario
	*
	*
	* Función que borra el usuario
	*
	* @param string $IDUse
	*
	*
	* @return boolean $Delete
	*/
	public static function borrarUsuario($IDUser){
		
		//Realizamos la consulta a la base de datos
		$delete=UsuarioPDO::borrarUsuario($IDUser);

		//Devolvemos la variable
        return $delete;
  }

      /**
     * 
     * 
     * Funcion que modifica un  usuario 
     * 
     * 
    * @param string $IDUser
	* @param string $Pass
	* @param string $Nom
	* @param string $Apell
	* @param string $Fecha_Nac
	* @param string $Email
       *  
     */
    public static function modificarUsuario($Pass,$Nom,$Apell,$Fecha_Nac,$Email,$IDUser){
    	
     	$Modificar=UsuarioPDO::modificarUsuario($Pass,$Nom,$Apell,$Fecha_Nac,$Email,$IDUser);

        return $Modificar;
     }
    
   

}

 ?>