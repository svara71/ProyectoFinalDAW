<?php 
require_once ('DBPDO.php');
/**
*	clase UsuarioPDO
*
* Clase que hace la llamada a la clase DBPDO con lo que le envia Usuario
*
*
* @author Samuel 
*
*
*
*/
class UsuarioPDO{


	/**
	*
	* Función validarUsuario
	*
	*
	* Función que valida si un usuario existe en la base de datos
	*
	* @param string $IDUser
	* @param string $Pass
	*
	*
	* @return array $arrayUsuario
	*/
	public static function validarUsuario($IDUser,$Pass){

	  //Inicializa un array
      $arrayUsuario=[];
      //Consulta a la base de datos
      $consulta="select * from Usuario where IDUser=? AND Pass=?";

      //Llamada al el metodo de DBPDO ejecutar consulta
      $resultado=DBPDO::ejecutaConsulta($consulta,[$IDUser,$Pass]);
       //Devuelve los resultados de la consulta
      $usuario= $resultado ->fetchObject();

      //Si al columna
      if($resultado->rowCount()){
       
          //Almacena el codigo
          $arrayUsuario['IDUser'] = $usuario->IDUser;
          //Almacena la contraseña
          $arrayUsuario['Pass'] = $usuario->Pass;
          //Almacena la nombre
          $arrayUsuario['Nom'] = $usuario->Nom;
          //Almacena el apellidos
          $arrayUsuario['Apell'] = $usuario->Apell;
          //Almacena el Fecha nacimiento
          $arrayUsuario['Fecha_Nac'] = $usuario->Fecha_Nac;
          //Almacena el Email
          $arrayUsuario['Email'] = $usuario->Email;
          //Almacena el Email
          $arrayUsuario['Activo'] = $usuario->Activo;
            //Almacena el Email
          $arrayUsuario['Admin'] = $usuario->Admin;
         
      }
      //Array que los campos de un usuario 
      return $arrayUsuario;

	}

	/**
	*
	* Función validarUsuario
	*
	*
	* Función que añade un nuevo usuario a la base de datos
	*
	* @param string $IDUser
	* @param string $Pass
	* @param string $Nom
	* @param string $Apell
	* @param string $Fecha_Nac
	* @param string $Email
	*
	*
	* @return boolean $usuarioInsert
	*/
	public static function insertarUsuario($IDUser,$Pass,$Nom,$Apell,$Fecha_Nac,$Email){
		//Declaramos un a variable boolean a False
		$usuarioInsert=false;

		//Consulta a la Base de Datos
		$consulta="insert into Usuario (IDUser,Pass,Nom,Apell,Fecha_Nac,Email) value(?,?,?,?,?,?)";
		
		//Realizamos la consulta a la base de datos
		$resultado=DBPDO::ejecutaConsulta($consulta,[$IDUser,$Pass,$Nom,$Apell,$Fecha_Nac,$Email]);

		//SI no es null
		if($resultado){
			//Cambiamos el valor a true
			$usuarioInsert=true;
		}
		//Devolvemos la variable
        return $usuarioInsert;
  }


	/**
	*
	* Función activoDesactivo
	*
	*
	* Función que activa o desactiva la cuenta
	*
	* @param string $IDUser
	* @param string $Activo
	*
	*
	* @return boolean $usuarioUpadate
	*/
	public static function activoDesactivo($Activo,$IDUser){
		//Declaramos un a variable boolean a False
		$usuarioUpdate=false;

		//Consulta a la Base de Datos
		$consulta="Update Usuario Set Activo=? where IDUser=?";
		
		//Realizamos la consulta a la base de datos
		$resultado=DBPDO::ejecutaConsulta($consulta,[$Activo,$IDUser]);

		//SI no es null
		if($resultado){
			//Cambiamos el valor a true
			$usuarioUpdate=true;
		}
		//Devolvemos la variable
        return $usuarioUpdate;
  }

  	/**
	*
	* Función borrarUsuario
	*
	*
	* Función que borra el usuario
	*
	* @param string $IDUse
	*
	*
	* @return boolean $usuarioDelete
	*/
	public static function borrarUsuario($IDUser){
		//Declaramos un a variable boolean a False
		$usuarioDelete=false;

		//Consulta a la Base de Datos
		$consulta="delete from Usuario where IDUser=?";
		
		//Realizamos la consulta a la base de datos
		$resultado=DBPDO::ejecutaConsulta($consulta,[$IDUser]);

		//SI no es null
		if($resultado){
			//Cambiamos el valor a true
			$usuarioDelete=true;
		}
		//Devolvemos la variable
        return $usuarioDelete;
  }

     /**
     * 
     * 
     * Funcion que modifica un  usuario 
     * 
     * 
    * @param string $IDUser
	* @param string $Pass
	* @param string $Nom
	* @param string $Apell
	* @param string $Fecha_Nac
	* @param string $Email
       *  
     */
    public static function modificarUsuario($Pass,$Nom,$Apell,$Fecha_Nac,$Email,$IDUser){
      $usuarioModificar=false;

      $consulta="update Usuario set Pass=? ,Nom=? ,Apell=? ,Fecha_Nac=? ,Email=? where IDUser=?";
      $resultado=DBPDO::ejecutaConsulta($consulta,[$Pass,$Nom,$Apell,$Fecha_Nac,$Email,$IDUser]);

      if($resultado){
        $usuarioModificar=true;
      }

        return $usuarioModificar;
     }
    
   
  
}





 ?>