<?php 
/**
 * 
 * Clase Compra
 * 
 * Clase que almacena los atributos de Compra
 * 
 *  @author Samuel Vara García
 * 
 * 	@version 1.0.0
 * 
 */
require_once 'DBPDO.php';
class CompraPDO{


	/**
	*
	* Función listarLibrosComprados
	*
	*
	*
	* @param string $IDUser
	*
	*
	* @return  $matrizLibrosComprados;
	*/

	public static function listarLibrosComprados($IDUser){

		//Inicializa una matriz
		$matrizLibrosComprados=[];

		//Consulta a la base de datos
		$consulta="Select * from Compra Inner Join Libro on Compra.Ref_Libro = Libro.Ref_Libro where IDUser=?";

		//Llamada al el metodo de DBPDO ejecutar consulta
		$resultado=DBPDO::ejecutaConsulta($consulta,[$IDUser]);

		//Si la consulta devuelve algun valor 
		if($resultado->rowCount()){
			//Lo metemos en la matriz
			 $matrizLibrosComprados= $resultado ->fetchAll();		 
		}

		//Matriz que contiene varios arrays con los campos de un Libro
		return $matrizLibrosComprados;
	}



	/**
	*
	* Función buscarLibrosCompradosNom
	*
	*
	*
	* @param string $IDUser
	* @param string $Nom_libro
	* @param string $Autor
	* @param string $Ref_Libro
	*
	*
	* @return  $matrizLibrosComprados
	*/
	public static function buscarLibrosCompradosNom($IDUser,$Nom_Libro,$Auto,$Ref_Libro){

		//Inicializa una matriz
		$matrizLibrosComprados=[];

		//Consulta a la base de datos
		$consulta="Select * from Compra Inner Join Libro on Compra.Ref_Libro = Libro.Ref_Libro where IDUser=? and (Libro.Nom_Libro like concat('%',?,'%') OR Libro.Autor like concat('%',?,'%') OR Libro.Ref_Libro like concat('%',?,'%'))";

		//Llamada al el metodo de DBPDO ejecutar consulta
		$resultado=DBPDO::ejecutaConsulta($consulta,[$IDUser,$Nom_Libro,$Auto,$Ref_Libro]);

		//Si la consulta devuelve algun valor 
		if($resultado->rowCount()){
			//Lo metemos en la matriz
			 $matrizLibrosComprados= $resultado ->fetchAll();		 
		}

		//Matriz que contiene varios arrays con los campos de un Libro
		return $matrizLibrosComprados;
	}
	
	
	/**
	*
	* Función buscarLibrosCompradosGenero
	*
	*
	*
	* @param string $IDUser
	* @param string $Generos
	*
	*
	* @return  $matrizLibrosComprados
	*/
	public static function buscarLibrosCompradosGenero($IDUser,$Generos){

		//Inicializa una matriz
		$matrizLibrosComprados=[];

		//Consulta a la base de datos
		$consulta="Select * from Compra Inner Join Libro on Compra.Ref_Libro = Libro.Ref_Libro where IDUser=? and Libro.Generos like concat('%',?,'%')";

		//Llamada al el metodo de DBPDO ejecutar consulta
		$resultado=DBPDO::ejecutaConsulta($consulta,[$IDUser,$Generos]);

		//Si la consulta devuelve algun valor 
		if($resultado->rowCount()){
			//Lo metemos en la matriz
			 $matrizLibrosComprados= $resultado ->fetchAll();		 
		}

		//Matriz que contiene varios arrays con los campos de un Libro
		return $matrizLibrosComprados;
	}
	
	
	
	/**
	*
	* Función comprarLibro
	*
	*
	*
	* @param string $IDUser
	*
	*
	* @return  
	*/

	public static function comprarLibro($Ref_Libro,$IDUser,$Fecha_Compra){

	  //Inicializa un array
      $libroComprado=false;
      //Consulta a la base de datos
      $consulta="Insert into Compra (Ref_Libro,IDUser,Fecha_Compra) value(?,?,?)";

      //Llamada al el metodo de DBPDO ejecutar consulta
      $resultado=DBPDO::ejecutaConsulta($consulta,[$Ref_Libro,$IDUser,$Fecha_Compra]);
    
      //Si la columna
      if($resultado){
       
		$libroComprado=true;
         
      }
      //Array que los campos de un usuario 
      return $libroComprado;

	}
}

?>