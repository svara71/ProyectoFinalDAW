<?php 
//Uso de la clase de acceso a la Base de Datos
require_once 'DBPDO.php';

/**
*
*
* Clase que hace la llamada a la clase DBPDO con lo que le envia Libro
*
*
* @author Samuel 
*
*
*
*/
class LibroPDO{


	/**
	*
	* Función buscarLibroNom
	*
	*
	* Función que busca los libros por el titulo, el autor 
	*
	* @param string $Nom_Libro
	* @param string $Autor
	*
	*
	* @return matriz $matrizLibros
	*/

	public static function buscarLibro($Nom_Libro,$Autor,$Ref_Libro){

		//Inicializa una matriz
		$matrizLibros=[];

		//Consulta a la base de datos
		$consulta="select distinct * from Libro where Nom_Libro like concat('%',?,'%') OR Autor like concat('%',?,'%') OR Ref_Libro like concat('%',?,'%')";

		//Llamada al el metodo de DBPDO ejecutar consulta
		$resultado=DBPDO::ejecutaConsulta($consulta,[$Nom_Libro,$Autor,$Ref_Libro]);

		//Si la consulta devuelve algun valor 
		if($resultado->rowCount()){
			//Lo metemos en la matriz
			 $matrizLibros= $resultado ->fetchAll();		 
		}

		//Matriz que contiene varios arrays con los campos de un Libro
		return $matrizLibros;
	}



	/**
	*
	* Función buscarLibroGeneros
	*
	*
	* Función que busca los libros por el genero 
	*
	* @param string $Generos
	*
	*
	* @return matriz $matrizLibros
	**/

	public static function buscarLibroGeneros($Generos){

		//Inicializa una matriz
		$matrizLibros=[];

		//Consulta a la base de datos
		$consulta="select distinct * from Libro where Generos like concat('%',?,'%')";

		//Llamada al el metodo de DBPDO ejecutar consulta
		$resultado=DBPDO::ejecutaConsulta($consulta,[$Generos]);

		//Si la consulta devuelve algun valor 
		if($resultado->rowCount()){
			//Lo metemos en la matriz
			$matrizLibros= $resultado->fetchAll();   
		}

		//Matriz que contiene varios arrays con los campos de un Libro
		return $matrizLibros;
	}


	/**
	*
	* Función buscarLibroGeneros
	*
	*
	* Función que busca los libros por el genero 
	*
	* @param string $Generos
	*
	*
	* @return matriz $matrizLibros
	*/
	public static function listarLibroNew(){

		//Inicializa una matriz
		$matrizLibros=[];

		//Consulta a la base de datos
		$consulta="select * from Libro where YEARWEEK(Fecha_Up) = YEARWEEK(CURDATE())";

		//Llamada al el metodo de DBPDO ejecutar consulta
		$resultado=DBPDO::ejecutaConsulta($consulta,[]);

		//Si la consulta devuelve algun valor 
		if($resultado->rowCount()){
			//Lo metemos en la matriz
			$matrizLibros= $resultado->fetchAll();
		}

		//Matriz que contiene varios arrays con los campos de un Libro
		return $matrizLibros;
	}

	public static function buscarLibroRef($Ref_Libro){

             //Inicializa un array
            $arrayLibro=[];

            //Consulta a la base de datos
            $consulta="select * from Libro where Ref_Libro=?";

            //Llamada al el metodo de DBPDO ejecutar consulta
            $resultado=DBPDO::ejecutaConsulta($consulta,[$Ref_Libro]);

            $libro= $resultado ->fetchObject();
            //Si al columna
            if($resultado->rowCount()){
             
               $arrayLibro['Ref_Libro'] = $libro->Ref_Libro;
               $arrayLibro['Nom_Libro'] = $libro->Nom_Libro;
               $arrayLibro['Autor'] = $libro->Autor;
               $arrayLibro['Num_Pag'] = $libro->Num_Pag;
               $arrayLibro['Fecha_Publi'] = $libro->Fecha_Publi;
               $arrayLibro['Sinopsis'] = $libro->Sinopsis;
               $arrayLibro['Portada'] = $libro->Portada;
               $arrayLibro['Generos'] = $libro->Generos;
               $arrayLibro['Fecha_Up'] = $libro->Fecha_Up;
               $arrayLibro['Archivo'] = $libro->Archivo;
               $arrayLibro['Precio'] = $libro->Precio;

            }

            //Array que los campos de un Libro
            return $arrayLibro;
      }



      /**
	*
	* Función Subir Libro
	*
	*
	* Función que añade un nuevo libro a la base de datos
	*
	* @param string $Ref_Libro
	* @param string $Nom_Libro
	* @param string $Autor
	* @param string $Num_Pag
	* @param date $Fecha_Publi
	* @param string $Sinopsis
	* @param string $Portada
	* @param string $Generos
	* @param date $Fecha_Up
	* @param string $Archivo
	* @param string $Precio
	* 
	*
	*
	* @return boolean $libroInsert
	*/
	public static function insertarLibro($Ref_Libro, $Nom_Libro, $Autor, $Num_Pag, $Fecha_Publi, $Sinopsis, $Portada, $Generos, $Fecha_Up, $Archivo, $Precio){
		//Declaramos un a variable boolean a False
		$libroInsert=false;

		//Consulta a la Base de Datos
		$consulta="insert into Libro (Ref_Libro, Nom_Libro, Autor, Num_Pag, Fecha_Publi, Sinopsis, Portada, Generos, Fecha_Up, Archivo, Precio) value(?,?,?,?,?,?,?,?,?,?,?)";
		
		//Realizamos la consulta a la base de datos
		$resultado=DBPDO::ejecutaConsulta($consulta,[$Ref_Libro, $Nom_Libro, $Autor, $Num_Pag, $Fecha_Publi, $Sinopsis, $Portada, $Generos, $Fecha_Up, $Archivo, $Precio]);

		//SI no es null
		if($resultado){
			//Cambiamos el valor a true
			$libroInsert=true;
		}
		//Devolvemos la variable
        return $libroInsert;
  }



      /**
	*
	* Función modificar Libro
	*
	*
	* Función que modifica un nuevo libro a la base de datos
	*
	* @param string $Ref_Libro
	* @param string $Nom_Libro
	* @param string $Autor
	* @param string $Num_Pag
	* @param date $Fecha_Publi
	* @param string $Sinopsis
	* @param string $Portada
	* @param string $Generos
	* @param date $Fecha_Up
	* @param string $Archivo
	* @param string $Precio
	* 
	*
	*
	* @return boolean $libroUpdate
	*/
	public static function modificarLibro( $Nom_Libro, $Autor, $Num_Pag, $Fecha_Publi, $Sinopsis, $Portada, $Generos, $Fecha_Up, $Archivo, $Precio,$Ref_Libro){
		//Declaramos un a variable boolean a False
		$libroUpdate=false;

		//Consulta a la Base de Datos
		$consulta="update Libro set Nom_Libro=?, Autor=?, Num_Pag=?, Fecha_Publi=?, Sinopsis=?, Portada=?, Generos=?, Fecha_Up=?, Archivo=?, Precio=? where Ref_Libro=?";
		
		//Realizamos la consulta a la base de datos
		$resultado=DBPDO::ejecutaConsulta($consulta,[ $Nom_Libro, $Autor, $Num_Pag, $Fecha_Publi, $Sinopsis, $Portada, $Generos, $Fecha_Up, $Archivo, $Precio, $Ref_Libro]);

		//SI no es null
		if($resUpdate){
			//Cambiamos el valor a true
			$libroUpdate=true;
		}
		//Devolvemos la variable
        return $libroUpdate;
  }


}
 ?>