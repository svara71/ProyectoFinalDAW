<?php 
/**
 * 
 * Fichero DBPDO.php
 * 
 * Fichero que permite las consultas sobre una base de Datos
 * 
 * 
 * @author Samuel Vara García
 * 
 * @package LibroWeb/model 
 * 
 */

	/**
	 * 
	 * Clase DBPDO
	 * 
	 * Clase que permite la conexion y la consulta a la abase de datos
	 * 
	 *  @author Samuel Vara García
	 * 
	 * 	@version 1.0.0
	 * 
	 */
	
	class DBPDO{

		/**
		 * 
		 * Funcion ejecutaConsulta
		 * 
		 * Funcion que se conecta y permite ejecutar una consulta sobre la BD
		 * 
		 * 
		 *  @author Samuel Vara García
		 * 
		 * 	@version 1.0.0
		 * 
		 * 	@param string $sentenciaSQL consulta a la DB
		 * 	@param array[] $parametros Array con los parametros de la consulta
		 * 
		 *  @return null|PDOStatement Null / ResultSet con la información del registro
		 * 
		 */

		public static function ejecutaConsulta($sentenciaSQL,$parametros){

			try{	
				//Inicia la conexion a la base de Datos
				// Uso en el Xampp Portable
				//$conexion = new PDO('mysql:host=localhost;dbname=LibroWeb','root','') ;

				//Uso Servidor del Instituto
				$conexion = new PDO('mysql:host=192.168.20.18;dbname=PROYECTO1_LibroWeb','PROYECTO1','paso') ;

				//Uso para Servidor Casa
				 //$conexion = new PDO('mysql:host=localhost;dbname=LibroWeb','libroweb','paso') ;
				 
				 
			
				//Definición de los atributos para que, si existe error, se lancen excepciones 
				 $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				//Se le pasa la consulta al prepare
				 $consultaPreparada = $conexion->prepare($sentenciaSQL);
     		  	//Se ejecuta la consulta preparada con los parametros que se le pasan 
				 $consultaPreparada->execute($parametros);
	      		
				//Si hay algun error
			 } 
			 catch (PDOException $e){ 
 				 //ConsultaPreparada pasa a  null
 				 $consultaPreparada=null;
 				 //Cierra la conexion
 				 unset($conexion);
			 }
		 		//Devuelve el resultado de la consulta
			   return $consultaPreparada;

		}
	}


 ?>