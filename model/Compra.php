<?php 
/**
 * 
 * Clase Compra
 * 
 * 
 *  @author Samuel Vara García
 * 
 * 	@version 1.0.0
 * 
 */
require_once 'CompraPDO.php';
class Compra{
	//Atributo de la Clase Compra
	protected $Ref_Compra;
	//Atributo de la Clase Compra
	protected $Ref_Libro;
	//Atributo de la Clase Compra
	protected $IDUser;
	//Atributo de la Clase Compra
	protected $Fecha_Compra;
	
	
	/**
	*
	* función getRef_Compra
	*
	* 
	*
	*/
	public function getRef_Compra(){
		return $this -> Ref_Compra;
	}
	
	/**
	*
	* función getRef_Libro
	*
	*
	*
	*/
	public function getRef_Libro(){
		return $this -> Ref_Libro;
	}
	
	/**
	*
	* función getIDUser
	*
	*
	*
	*/
	public function getIDUser(){
		return $this -> IDUser;
	}
	
	/**
	*
	* función getFecha_Compra
	*
	*
	*
	*/
	public function getFecha_Compra(){
		return $this -> Fecha_Compra;
	}
	


	/**
	*
	* función __construct
	*
	* Constructor de la la clase Compra
	*
	*/
	public  function __construct($Ref_Compra,$Ref_Libro,$IDUser,$Fecha_Compra)
	{
		$this->Ref_Compra = $Ref_Compra;
		$this->Ref_Libro = $Ref_Libro;
		$this->IDUser = $IDUser;
		$this->Fecha_Compra = $Fecha_Compra;
		
	}
	

	/**
	*
	* Función listarLibrosComprados
	*
	*
	* Función que busca los libros por el genero 
	*
	* @param string $IDUser
	*
	*
	* @return  
	*/
	public static function listarLibrosComprados($IDUser){


		//Recogemos los datos Recibidos De la clase LibroPDO en una Matriz
		$matrizLibrosComprado=CompraPDO::listarLibrosComprados($IDUser);
	
		//Devolvemos el Array de Objetos
		return $matrizLibrosComprado;
	}
	
	/**
	*
	* Función buscarLibrosCompradosNom
	*
	*
	* Función que busca los libros por el genero 
	*
	* @param string $IDUser
	*
	*
	* @return  
	*/
	public static function buscarLibrosCompradosNom($IDUser,$Nom_Libro,$Auto,$Ref_Libro){


		//Recogemos los datos Recibidos De la clase LibroPDO en una Matriz
		$matrizLibrosComprado=CompraPDO::buscarLibrosCompradosNom($IDUser,$Nom_Libro,$Auto,$Ref_Libro);
	
		//Devolvemos el Array de Objetos
		return $matrizLibrosComprado;
	}
	
	/**
	*
	* Función buscarLibrosCompradosGenero
	*
	*
	* Función que busca los libros por el genero 
	*
	* @param string $IDUser
	*
	*
	* @return  
	*/
	public static function buscarLibrosCompradosGenero($IDUser,$Generos){


		//Recogemos los datos Recibidos De la clase LibroPDO en una Matriz
		$matrizLibrosComprado=CompraPDO::buscarLibrosCompradosGenero($IDUser,$Generos);
	
		//Devolvemos el Array de Objetos
		return $matrizLibrosComprado;
	}

	/**
	*
	* Función buscarLibrosCompradosGenero
	*
	*
	* Función que busca los libros por el genero 
	*
	* @param string $IDUser
	*
	*
	* @return  
	*/
	public static function comprarLibro($Ref_Libro,$IDUser,$Fecha_Compra){
		//Recogemos la respuesta Recibida De la clase CompraPDO
		$insert=CompraPDO::comprarLibro($Ref_Libro,$IDUser,$Fecha_Compra);
		
		//la devolvemos
		return $insert;
	}
	
	}
 ?>