<?php 
/**
 * 
 * Clase Libro
 * 
 * Clase que almacena los atributos de Libro
 * 
 *  @author Samuel Vara García
 * 
 * 	@version 1.0.0
 * 
 */
require_once 'LibroPDO.php';
class Libro{
	//Atributo de la Clase Libro
	protected $Ref_Libro;
	//Atributo de la Clase Libro
	protected $Nom_Libro;
	//Atributo de la Clase Libro
	protected $Autor;
	//Atributo de la Clase Libro
	protected $Num_Pag;
	//Atributo de la Clase Libro
	protected $Fecha_Publi;
	//Atributo de la Clase Libro
	protected $Sinopsis;
	//Atributo de la Clase Libro
	protected $Portada;
	//Atributo de la Clase Libro
	protected $Generos;
	//Atributo de la Clase Libro
	protected $Fecha_Up;
	//Atributo de la Clase Libro
	protected $Archivo;
	//Atributo de la Clase Libro
	protected $Precio;
	
	/**
	*
	* función getRef_Libro
	*
	* Devuelve el valor de la referencia del libro
	*
	*/
	public function getRef_Libro(){
		return $this -> Ref_Libro;
	}
	
	/**
	*
	* función getNom_Libro
	*
	*Devuelve el valor de la nombre del libro
	*
	*/
	public function getNom_Libro(){
		return $this -> Nom_Libro;
	}
	
	/**
	*
	* función getAutor
	*
	*Devuelve el valor de la Autor del libro
	*
	*/
	public function getAutor(){
		return $this -> Autor;
	}
	
	/**
	*
	* función getNum_Pag
	*
	*Devuelve el valor de la Numero de paginas del libro
	*
	*/
	public function getNum_Pag(){
		return $this -> Num_Pag;
	}
	
	/**
	*
	* función getFecha_Publi
	*
	*Devuelve el valor de la Fecha de Publicación del libro
	*
	*/
	public function getFecha_Publi(){
		return $this -> Fecha_Publi;
	}
	
	/**
	*
	* función getSinopsis
	*
	*Devuelve el valor de la Sinopsis del libro
	*
	*/
	public function getSinopsis(){
		return $this -> Sinopsis;
	}
	
	/**
	*
	* función getPortada
	*
	*Devuelve el valor de la Portada del libro
	*
	*/
	public function getPortada(){
		return $this -> Portada;
	}
	
	/**
	*
	* función getGeneros
	*
	*Devuelve el valor de la Generos del libro
	*
	*/
	public function getGeneros(){
		return $this -> Generos;
	}
	
	/**
	*
	* función getFecha_Up
	*
	*Devuelve el valor de la Fecha que se subio a la pagina del libro
	*
	*/
	public function getFecha_Up(){
		return $this -> Fecha_Up;
	}

	/**
	*
	* función getFecha_Up
	*
	*Devuelve el valor de la Fecha que se subio a la pagina del libro
	*
	*/
	public function getArchivo(){
		return $this -> Archivo;
	}

	/**
	*
	* función getFecha_Up
	*
	*Devuelve el valor de la Fecha que se subio a la pagina del libro
	*
	*/
	public function getPrecio(){
		return $this -> Precio;
	}

	/**
	*
	* función __construct
	*
	* Constructor de la la clase Libro
	*
	*/
	public  function __construct($Ref_Libro,$Nom_Libro,$Autor,$Num_Pag,$Fecha_Publi,$Sinopsis,$Portada,$Generos,$Fecha_Up,$Archivo,$Precio)
	{
		$this->Ref_Libro = $Ref_Libro;
		$this->Nom_Libro = $Nom_Libro;
		$this->Autor = $Autor;
		$this->Num_Pag = $Num_Pag;
		$this->Fecha_Publi = $Fecha_Publi;
		$this->Sinopsis = $Sinopsis;
		$this->Portada = $Portada;
		$this->Generos = $Generos;
		$this->Fecha_Up = $Fecha_Up;
		$this->Archivo = $Archivo;
		$this->Precio = $Precio;
	}
	
	/**
	*
	* Función buscarLibroNom
	*
	*
	* Función que busca los libros por el titulo, el autor 
	*
	* @param string $Nom_Libro
	* @param string $Autor
	*
	*
	* @return array de objetos  $ArrayObj
	*/
	public static function buscarLibro($Nom_Libro,$Autor,$Ref_Libro){

		//Inicializamos el objeto libro a Null
		$ObjLibro=null;
		//Creamos un Array vacio
		$ArrayObj=[];
		
		//Recogemos los datos Recibidos De la clase LibroPDO en una Matriz
		$matrizLibros=LibroPDO::buscarLibro($Nom_Libro,$Autor,$Ref_Libro);

		//Recorremos la matrizLibros
		foreach ($matrizLibros as $arrayLibros) {
			//Creamos un Nuevo objeto con los datos recogidos deun array de la matriz
			$ObjLibro= new Libro($arrayLibros['Ref_Libro'],$arrayLibros['Nom_Libro'],$arrayLibros['Autor'],$arrayLibros['Num_Pag'],$arrayLibros['Fecha_Publi'],$arrayLibros['Sinopsis'],$arrayLibros['Portada'],$arrayLibros['Generos'],$arrayLibros['Fecha_Up'],$arrayLibros['Archivo'],$arrayLibros['Precio']);
			
			//Añadimos el objeto a el array
			$ArrayObj[]=$ObjLibro;
		}
		//Devolvemos el Array de Objetos
		return $ArrayObj;
	}

	/**
	*
	* Función buscarLibroGeneros
	*
	*
	* Función que busca los libros por el genero 
	*
	* @param string $Generos
	*
	*
	* @return Array de Objetos $ObjLibro
	*/
	public static function buscarLibroGeneros($Generos){

		//Inicializamos el objeto libro a Null
		$ObjLibro=null;
		//Creamos un Array vacio
		$ArrayObj=[];
		
		//Recogemos los datos Recibidos De la clase LibroPDO en una Matriz
		$matrizLibros=LibroPDO::buscarLibroGeneros($Generos);
		
		//Recorremos la matrizLibros
		foreach ($matrizLibros as $arrayLibros) {
			
			//Creamos un Nuevo objeto con los datos recogidos deun array de la matriz
			$ObjLibro= new Libro($arrayLibros['Ref_Libro'],$arrayLibros['Nom_Libro'],$arrayLibros['Autor'],$arrayLibros['Num_Pag'],$arrayLibros['Fecha_Publi'],$arrayLibros['Sinopsis'],$arrayLibros['Portada'],$arrayLibros['Generos'],$arrayLibros['Fecha_Up'],$arrayLibros['Archivo'],$arrayLibros['Precio']);

			//Añadimos el objeto a el array
			$ArrayObj[]=$ObjLibro;
		}
		//Devolvemos el Array de Objetos
		return $ArrayObj;
	}



	/**
	*
	* Función buscarLibroGeneros
	*
	*
	* Función que busca los libros por el genero 
	*
	* @param string $Generos
	*
	*
	* @return  Array de Objetos $ObjLibro
	*/
	public static function listarLibroNew(){

		//Inicializamos el objeto libro a Null
		$ObjLibro=null;
		
		//Creamos un Array vacio
		$ArrayObj=[];
		
		//Recogemos los datos Recibidos De la clase LibroPDO en una Matriz
		$matrizLibros=LibroPDO::listarLibroNew();
		
		//Recorremos la matrizLibros
		foreach ($matrizLibros as $arrayLibros) {
			
			//Creamos un Nuevo objeto con los datos recogidos deun array de la matriz
			$ObjLibro= new Libro($arrayLibros['Ref_Libro'],$arrayLibros['Nom_Libro'],$arrayLibros['Autor'],$arrayLibros['Num_Pag'],$arrayLibros['Fecha_Publi'],$arrayLibros['Sinopsis'],$arrayLibros['Portada'],$arrayLibros['Generos'],$arrayLibros['Fecha_Up'],$arrayLibros['Archivo'],$arrayLibros['Precio']);

			
			//Añadimos el objeto a el array
			$ArrayObj[]=$ObjLibro;
		}
		//Devolvemos el Array de Objetos
		return $ArrayObj;
	}
	
	
	public static function buscarLibroRef($Ref_Libro){


		$ObjLibro=null;
		

		$arrayLibros=LibroPDO::buscarLibroRef($Ref_Libro);

		
		if($arrayLibros){
			$ObjLibro= new Libro($arrayLibros['Ref_Libro'],$arrayLibros['Nom_Libro'],$arrayLibros['Autor'],$arrayLibros['Num_Pag'],$arrayLibros['Fecha_Publi'],$arrayLibros['Sinopsis'],$arrayLibros['Portada'],$arrayLibros['Generos'],$arrayLibros['Fecha_Up'],$arrayLibros['Archivo'],$arrayLibros['Precio']);

		}
	

		return $ObjLibro;
	}


      /**
	*
	* Función Subir Libro
	*
	*
	* Función que añade un nuevo libro a la base de datos
	*
	* @param string $Ref_Libro
	* @param string $Nom_Libro
	* @param string $Autor
	* @param string $Num_Pag
	* @param date $Fecha_Publi
	* @param string $Sinopsis
	* @param string $Portada
	* @param string $Generos
	* @param date $Fecha_Up
	* @param string $Archivo
	* @param string $Precio
	* 
	*
	*
	* @return boolean $libroInsert
	*/
	public static function insertarLibro($Ref_Libro, $Nom_Libro, $Autor, $Num_Pag, $Fecha_Publi, $Sinopsis, $Portada, $Generos, $Fecha_Up, $Archivo, $Precio){
		//Declaramos un a variable boolean a False
		$libroInsert=LibroPDO::insertarLibro($Ref_Libro, $Nom_Libro, $Autor, $Num_Pag, $Fecha_Publi, $Sinopsis, $Portada, $Generos, $Fecha_Up, $Archivo, $Precio);

		
        return $libroInsert;
  	}


      /**
	*
	* Función modificar Libro
	*
	*
	* Función que modificar un nuevo libro a la base de datos
	*
	* @param string $Ref_Libro
	* @param string $Nom_Libro
	* @param string $Autor
	* @param string $Num_Pag
	* @param date $Fecha_Publi
	* @param string $Sinopsis
	* @param string $Portada
	* @param string $Generos
	* @param date $Fecha_Up
	* @param string $Archivo
	* @param string $Precio
	* 
	*
	*
	* @return boolean $libroUpdate
	*/
	public static function modificarLibro($Nom_Libro, $Autor, $Num_Pag, $Fecha_Publi, $Sinopsis, $Portada, $Generos, $Fecha_Up, $Archivo, $Precio,$Ref_Libro){
		//Declaramos un a variable boolean a False
		$libroUpdate=LibroPDO::modificarLibro($Nom_Libro, $Autor, $Num_Pag, $Fecha_Publi, $Sinopsis, $Portada, $Generos, $Fecha_Up, $Archivo, $Precio,$Ref_Libro);

		
        return $libroUpdate;
  	}
	}
 ?>