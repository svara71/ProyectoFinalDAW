<?php 
require_once 'model/Usuario.php';

if(count(scandir("./Images"))>2){
	//Inculuimos la libreria de funciones
	include 'LibreriaFunciones.php';

	vaciarCarpeta("./Images");
}
//Si existe la Directorio
if(isset($_SESSION['directorio'])){
	//Borramos directorio
	unset($_SESSION['directorio']);
}
//Si no exite una sesion 
if (!isset($_SESSION['usuario'])){
	
	//Si pulsas Iniciar
	$desactivada=false;
	//$userCont=null;
	
	if (isset($_POST['Iniciar'])){
		include 'LibreriaFunciones.php';
		$inicioCorrecto=true;

		//Validacion de que el campo  Usuario  no este vacio
		if(validarCampoVacio($_POST['user']) == 0){
			$error['errorIDuser']='Campo Usuario Vacio';
			$inicioCorrecto=false;
		} 
		
		//Validacion de que el campo  Contrasiña  no este vacio
		if(validarCampoVacio($_POST['pass']) == 0){
			$error['errorPass']='Campo Contraseña Vacia';
			$inicioCorrecto=false;
		}
		
		if($inicioCorrecto){
			//hash de la contraseña
			$passResumen=hash("sha256",$_POST['pass']);
			//Usamos la funcion validarUsuario con el valor del user y el de la contraseña hasheada 
			$_SESSION['activoDesactivo']=Usuario::validarUsuario($_POST['user'],$passResumen);
		
			//SI el resultado de la consulta no es null
			if(!is_null($_SESSION['activoDesactivo'])){
				//Comprobamos si la cuenta esta activa o no 
				if($_SESSION['activoDesactivo']->getActivo()==0){
					
					//$userCont=$_SESSION['activoDesactivo']->getIDUser();
					//Si esta desactivada cambiamos a true
					$desactivada=true;
				//si no	
				}else{
					//Me temos la información de $_SESSION['activoDesactivo'] en $_SESSION['usuario']
					$_SESSION['usuario']=$_SESSION['activoDesactivo'];
					//Borramos $_SESSION['activoDesactivo']
					unset($_SESSION['activoDesactivo']);

					if($_SESSION['usuario']->getAdmin()==1){
						//Redireccionas a la pagina del usuario
						header('Location: index.php?location=paguserperfil');
					}else{
						//Redireccionas a la pagina del usuario
						header('Location: index.php?location=paguser');
					}
					
				}
			}else{
					//si hay error en la consulta 
					$error['errorNoUser']='El usuario no exite o hay un error en la contraseña o el usuario';
			}
		}

	}
	//Si pulsamos SI
	if(isset($_POST['Si'])){
		//Usamos la funcion activaDesactiva para activar la cuenta
		Usuario::activoDesactivo(1,$_SESSION['activoDesactivo']->getIDUser());
		//Me temos la información de $_SESSION['activoDesactivo'] en $_SESSION['usuario']
		$_SESSION['usuario']=$_SESSION['activoDesactivo'];
		//Borramos de la base de datos
		unset($_SESSION['activoDesactivo']);
		//Redireccionas a la pagina del usuario
		header('Location: index.php?location=login');
	}
	//Si pulsamos NO
	if(isset($_POST['No'])){
		//Redireccionas a la pagina del usuario
		header('Location: index.php?location=login');
	}

	//Si pulsamos Registro
	if (isset($_POST['Registro'])) {
		//Añadir comprabación de las campos
		$registroCorrecto=true;
		//Inculimos Libreria de Funciones
		include 'LibreriaFunciones.php';

		//Validacion de que el campo  Usuario  no este vacio
		if(validarCampoVacio($_POST['iduserNEW']) == 0){
			$error['errorIDuserNew']='Campo Usuario Vacio';
			$registroCorrecto=false;
		} 
		
		//Validacion de que el campo  Contrasiña  no este vacio
		if(validarCampoVacio($_POST['passNEW']) == 0){
			$error['errorPassNew']='Campo Contraseña Vacia';
			$registroCorrecto=false;
		}

		//Validacion de que el campo  Nombre  no este vacio
		if(validarCampoVacio($_POST['nomNEW']) == 0){
			$error['errorNomNew']='Campo Nombre Vacio';
			$registroCorrecto=false;
		}else{
			//Validacion de que el campo  Nombre solo sena letras
			if(validarCampoTexto($_POST['nomNEW']) == 0){
				$error['errorNomNew']='El campo Nombre solo puede contener letras';
				$registroCorrecto=false;
			}
		}
		//Validacion de que el campo  Apellidos no este vacio
		if(validarCampoVacio($_POST['apellNEW']) == 0){
			$error['errorApellNew']='Campo Apellidos Vacio';
			$registroCorrecto=false;
		}else{
			//Validacion de que el campo  Apellidos solo sena letras
			if(validarCampoTexto($_POST['apellNEW']) == 0){
				$error['errorApellNew']='El campo Apellidos solo puede contener letras';
				$registroCorrecto=false;
			}
		}

		//Validacion de que el campo  Fecha Nacimiento  no este vacio
		if(validarCampoVacio($_POST['FechaNacNEW']) == 0){
			$error['errorFechaNacNew']='Campo Fecha Nacimiento esta Vacio';
			$registroCorrecto=false;
		}else{
			//Validacion de  que el campo Fecha naciemiento no sea una fecha superior a la actual
			if(validarCampoFecha($_POST['FechaNacNEW'])==0){
				$modificadoCorrecto=false;
				$error['errorFechaNacNew']='Error formato Fecha "AAAA-MM-DD"';
			}else if(validarCampoFecha($_POST['FechaNacNEW'])==2){
				$modificadoCorrecto=false;
				$error['errorFechaNacNew']='Fecha Incorrecta';
				
			}else if(validarFechaAnt($_POST['FechaNacNEW']) == 0){
				$error['errorFechaNacNew']='El campo Fecha Nacimiento tiene que ser anterior a la fecha actual';
				$modificadoCorrecto=false;
			}
		}

		//Validacion de que el campo  Fecha Nacimiento  no este vacio
		if(validarCampoVacio($_POST['emailNEW']) == 0){
			$error['errorEmailNew']='Campo Email esta Vacio';
			$registroCorrecto=false;
		}else{
			if(validarEmail($_POST['emailNEW']) == 0){
				$error['errorEmailNew']='Formato incorrecto del Email ';
				$registroCorrecto=false;
			}
		}

		//SI no hay error en los campos
		if($registroCorrecto){
			//Hash de el passNew
			$passNewResumen=hash("sha256",$_POST['passNEW']);
			//LLamada a la funcion insertarUsuario con los valores de los campos
			$registro=Usuario::insertarUsuario($_POST['iduserNEW'],$passNewResumen,$_POST['nomNEW'],$_POST['apellNEW'],$_POST['FechaNacNEW'],$_POST['emailNEW']);
			
			//Si la funcion devuelve true
			if($registro){
				//Iniciamos sesion
				$_SESSION['usuario']=Usuario::validarUsuario($_POST['iduserNEW'],$passNewResumen);
				//Redireccionas a la pagina del usuario
				header('Location: index.php?location=paguser');
			//Sino
			}else{
				//Cargamos un error
				$error['errorUserExist']='El usuario ya existe en la Base de Datos';
			}
			

		}

	}
	//Inluimos el layout
	include 'view/layout.php';

//Si existe un usuario conectado
}else{
	//Redireccionas a la pagina del inicio
	header('Location: index.php?location=paguser');
}
 ?>
