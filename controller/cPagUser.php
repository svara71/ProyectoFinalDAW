<?php 	
require_once 'model/Compra.php';

if(count(scandir("./Images"))>2){
	//Inculuimos la libreria de funciones
	include 'LibreriaFunciones.php';

	vaciarCarpeta("./Images");
}
//Si existe la Directorio
if(isset($_SESSION['directorio'])){
	//Borramos directorio
	unset($_SESSION['directorio']);
}

//Si exite un suario conectado	
if (isset($_SESSION['usuario'])) {

	//Guardamos el valor de IDuser en una variable
	$user=$_SESSION['usuario']->getIDUser();
	//Usamos una funcion de Compra para ver todos los libros comprados
	$_SESSION['listaLibrosComprados']=Compra::listarLibrosComprados($user);

	//Si se pulsa buscar
	if(isset($_REQUEST['buscar'])){
		unset($_SESSION['tipoCom']);
		$_SESSION['nomCom']=$_POST['nom'];
		$_SESSION['buscarLibrosComprados']=Compra::buscarLibrosCompradosNom($user,$_POST['nom'],$_POST['nom'],$_POST['nom']);
		//Redireccionas a la pagina del pagina usuario
		header('Location: index.php?location=paguser');
	}

	//Si se pulsa un genero
	if(isset($_GET['Genero'])){
		unset($_SESSION['nomCom']);
		$_SESSION['tipoCom']=$_GET['tipo'];
		//Se realiza una consulta a la funcion buscarlibrocompradosGeneros con el valor de el campo tipo y si mete el resultado en la variable de Sesion
		$_SESSION['buscarLibrosComprados']=Compra::buscarLibrosCompradosGenero($user,$_GET['tipo']);
		//Redireccionas a la pagina de pagina usuario
		header('Location: index.php?location=paguser');
	}

	//Si se pulsa infolibro
	if(isset($_GET['InfoLibro'])){
		//Guardamos el valor de la referencia en una variable
		$ref=$_GET['Referencia'];
		//Redireccionas a la pagina de infolibro con la variable que guarda la referencia
		header("Location: index.php?location=infolibro&Referencia=$ref");
	}

	include 'view/layout.php';
//Si no 
}else{
	//Redireccionas a la pagina del login
	header('Location: index.php?location=login');
}
?>