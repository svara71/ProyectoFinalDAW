<?php 
require_once 'model/Compra.php';
require_once 'model/Libro.php';

if(count(scandir("./Images"))>2){
	//Inculuimos la libreria de funciones
	include 'LibreriaFunciones.php';
	vaciarCarpeta("./Images");
}

//Si existe la Directorio
if(isset($_SESSION['directorio'])){
	//Borramos directorio
	unset($_SESSION['directorio']);
}


if(isset($_SESSION['usuario'])){
	//Si exixte la referencia
	if(isset($_GET['referencia'])){

			//Añadimos el valor de la referncia a una variable
			$refe=$_GET['referencia'];
						//Buscamos los libros comprados
			$libroCom=Compra::buscarLibrosCompradosNom($_SESSION['usuario']->getIDUser(),0,0,$refe);
			$libroACom=Libro::buscarLibroRef($refe);
				
				//Comprabamos si la referncia coincide con algun libro comprado.
				if($libroCom==null){	
					//Incluimos la el layout
					include 'view/layout.php';
				}else{
					//Rediriges a la pagian inicio usuario
					header("Location: index.php?location=infolibro&Referencia=$refe");
				}
	}else{
		//Rediriges a la pagian inicio
		header("Location: index.php?location=inicio");
	}

	//Si pulsas comprar
	if(isset($_POST['comprar'])){
		//Fecha de Hoy Formateada
		$Fecha_Compra=date("Y-m-d");
		//LLamas a la funcion de Compra COmprarLibro y le pasas la referncia el iduser y la fecha de hoy
		Compra::comprarLibro($_POST['ref'],$_POST['iduser'],$Fecha_Compra);
		
		$ref=$_POST['ref'];
		//Rediriges a la pagian del libro comprado
		header("Location: index.php?location=infolibro&Referencia=$ref");
	}
	//Si pulsas atras
	if(isset($_POST['Atras'])){
		
		$ref=$_POST['ref'];
		//Rediriges a la pagian del libro comprado
		header("Location: index.php?location=infolibro&Referencia=$ref");
	}

}else{
	//Rediriges a la pagian de login
	header("Location: index.php?location=login");
}

 ?>