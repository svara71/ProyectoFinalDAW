<?php 
//Incluimos las Clases 
require_once 'model/Libro.php';
require_once 'model/Usuario.php';
require_once 'model/Compra.php';

if(count(scandir("./Images"))>2){
	//Inculuimos la libreria de funciones
	include 'LibreriaFunciones.php';

	vaciarCarpeta("./Images");
}

//Si existe la Directorio
if(isset($_SESSION['directorio'])){
	//Borramos directorio
	unset($_SESSION['directorio']);
}

//Si existe la referencia
if(isset($_GET['Referencia'])){

	//Recuperamos el valor de la referencia
	$ref=$_GET['Referencia'];

	//Usamos la funcion Buscar libro por referencia y metemos los datos en una variable de session
	$correct=Libro::buscarLibroRef($ref);
	
	//SI correct no esta vacio
	if(!is_null($correct)){

		//Añadimos los valores a la sesion de buscar libro
		$_SESSION['buscarLibro']=$correct;
	
	//Si no
	}else{

		//Redirigimos a la pagina de inicio
		header("Location: index.php?location=inicio");
	}
		
}else{
	//Redirigimos a la pagina de inicio
	header("Location: index.php?location=inicio");
}

//Si se pulsa login
if(isset($_REQUEST['login'])){
	//Redireccionas a la pagina de login
	header('Location: index.php?location=login');
}

//Si se pulsa Comprar
if(isset($_GET['comprar'])){
	//Guardamos el valor de la referencia en una variable
	$ref02=$_GET['Referencia'];
	//Redireccionas a la pagina de infolibro con la variable que guarda la referencia
	header("Location: index.php?location=pagpago&referencia=$ref02");
}

//Si se pulsa ver
if(isset($_GET['ver'])){
	//Guardamos el valor de la referencia en una variable
	$ref01=$_GET['Referencia'];
	//Redireccionas a la pagina de infolibro con la variable que guarda la referencia
	header("Location: index.php?location=ver&referencia=$ref01");
}

//Si se pulsa ver1cap
if(isset($_GET['ver1cap'])){
	//Guardamos el valor de la referencia en una variable
	$ref01=$_GET['Referencia'];
	//Redireccionas a la pagina de infolibro con la variable que guarda la referencia
	header("Location: index.php?location=ver&referencia=$ref01&ver1cap");
}

//Si se pulsa editar
if(isset($_GET['editar'])){
	//Guardamos el valor de la referencia en una variable
	$ref01=$_GET['Referencia'];
	//Redireccionas a la pagina de infolibro con la variable que guarda la referencia
	header("Location: index.php?location=modlibro&referencia=$ref01");
}

//Si Exite una sesion
if(isset($_SESSION['usuario'])){
	//Declaramos a False
	$libroComprado=false;
	//Buscarmos en la tabla de compras si se a comprado el libro
	$libro=Compra::listarLibrosComprados($_SESSION['usuario']->getIDUser());
	//Recorremos 
	foreach($libro as $key){
		//Comprobamos si esta en la tabla
		if($key['Ref_Libro']==$ref){
			//Cambiamos a true si esta en la tabla
			$libroComprado=true;
		}
	}
}



//Incluimos el layout
include 'view/layout.php';
 ?>