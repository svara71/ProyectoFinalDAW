<?php 
require_once 'model/Compra.php';
require_once 'model/Libro.php';

//SI existe un usuario o esiste ver1cap 
if(isset($_SESSION['usuario']) || isset($_GET['ver1cap'])){
	//Si existe la referncia
	if(isset($_GET['referencia'])){
		
		//Si el usuario que existe es admin o exite ver1cap
		if((isset($_SESSION['usuario']) && $_SESSION['usuario']->getAdmin()==1) || isset($_GET['ver1cap'])){
			
			//Variable que almacena el libro si el usuario es admin y para ver un capitulo gratis
			$libroVerAdmin=Libro::buscarLibroRef($_GET['referencia']);

		//Si es un usuario mormal
		}else if(isset($_SESSION['usuario'])){
			//Variable que almacena el libro si el usuario es normal
			$libroVer=Compra::buscarLibrosCompradosNom($_SESSION['usuario']->getIDUser(),0,0,$_GET['referencia']);
			
		}
		

		$ver1cap=false;

		//Si el usuario es admin
		if((isset($_SESSION['usuario']) && $_SESSION['usuario']->getAdmin()==1) || isset($_GET['ver1cap'])){
				//Almacenamos la direccion
				$_SESSION['directorio']=$libroVerAdmin->getArchivo();
				//Almacenamos la el titulo del libro
				$titulo=$libroVerAdmin->getNom_Libro();

		//Si se a comprado el libro
		}else if($libroVer[0]['Ref_Libro']==$_GET['referencia']){
				//Almacenamos la direccion
				$_SESSION['directorio']=$libroVer[0]['Archivo'];
				//Almacenamos la el titulo del libro
				$titulo=$libroVer[0]['Nom_Libro'];
		//Sino		
		}else{
			//Rediriges a la pagian del libro
			header("Location: index.php?location=infolibro&Referencia=".$_GET['referencia']);
		}

	}else{
		//Rediriges a la pagian de login
		header("Location: index.php?location=login");
	}




$images=array_slice(scandir($_SESSION['directorio']."/OEBPS/Images"),2);
foreach($images as $archivos_images){
	
	copy($_SESSION['directorio']."/OEBPS/Images/".$archivos_images, "./Images/".$archivos_images);
}


//Leemos y almacenamos un xml 
 $xml = simplexml_load_file($_SESSION['directorio'].'/OEBPS/toc.ncx');
		 		
			//Array vacio
 			$text=[];
 			//Array Vacio
 			$file=[];
 			//Formato A true
 			$formato=true;

			//Variable que guarda valores del objeto xml navMap
			$navMap=$xml->navMap;	
 			//Variable que guarda valores del objeto xml navPoint
			$navPoint=$xml->navMap->navPoint[1];
			//Contamos los navPont que hay dentro del navMap
			$totalNavPoint=count($navMap->navPoint);

			if($totalNavPoint>2){
				$con=0;
				while ( $con < $totalNavPoint) {
					//Recorremos NavMap
					foreach ($navMap as $key ) {
					
						//Añadimos el hijo texto a un array 
						array_push($text,(string) $key->navPoint[$con]->navLabel->text);
						//Añadimos el hijo src a un array
						array_push($file,(string) $key->navPoint[$con]->content['src']);
						
					}
					$con=$con+1;
				}
			}else{
					//Recorremos Navpoint
					foreach ($navPoint as $key ) {
				
						//Añadimos el hijo texto a un array 
						array_push($text,(string) $key->navLabel->text);
						//Añadimos el hijo src a un array
						array_push($file,(string) $key->content['src']);
						$formato=false;
					}
			}
			

			//SI se declaro ver1cap  y formato es true
			if(isset($_GET['ver1cap']) && $formato){
				//Ver1cap pasa a true
				$ver1cap=true;

				//Truncamos y pasamos a unas variables 
				$fichero=array_slice($file, 2,4);
				$titulos=array_slice($text, 2, 4);

				

			//Si se declara ver1cap y formato es false
			}else if(isset($_GET['ver1cap']) && !$formato){
				//Ver1cap pasa a true
				$ver1cap=true;

				//Truncamos y pasamos a unas variables 
				$fichero=array_slice($file, 2,4);
				$titulos=array_slice($text, 2, 4);

			//SI formato es true
			}else if($formato){

				//pasamos a unas variables 
				$fichero=array_slice($file, 2);
				$titulos=array_slice($text, 2);

				

			}else{
				$fichero=array_slice($file,2);
				$titulos=array_slice($text,2);
			}
				
		
			
//Total de valores en el array
$total=count($titulos);

//Si se pulsa siguiente
if(isset($_GET['Siguiente'])){
	//Sacamos al posicion en la que se encuentra el array
	$pos=array_search($_SESSION['pagina'],$fichero);

	//Si al posicion es menor que el total -1
	if($pos==$total-1) {
		//Entonces redirigimos a la primera pagina
		$_SESSION['pagina']=$fichero[0];
	//sino
	}else{
		//Continuamos 
		$_SESSION['pagina']=$fichero[$pos+1];
	}
	
}else{
	//Si se pulsa anterior
	if(isset($_GET['Anterior'])){
		//Sacamos al posicion en la que se encuentra el array
		$pos=array_search($_SESSION['pagina'],$fichero);
		//Si la posicion es 0
		if($pos==0){
			//Permanecemoes en la pagina
			$_SESSION['pagina']=$fichero[0];
		//
		}else{
			//Sino retrocedemos 
			$_SESSION['pagina']=$fichero[$pos-1];	
		}
	
	}else{
		//Cuando se entra por primera vez se carpa el primer capitul0
		$_SESSION['pagina']=$fichero[0];
	}
}

//Si se pulsa un elemento del incice
if (isset($_GET['Indice'])) {
	
	//Sacamos al posicion del capitulo en la que se encuentra el array
	$pos=array_search($_GET['capitulo'],$fichero);
	//SI lo cargamos en el array
	$_SESSION['pagina']=$fichero[$pos];
}
	
//Si se pulsa infolibro
if(isset($_GET['InfoLibro'])){

	//Guardamos el valor de la referencia en una variable
	$ref=$_GET['referencia'];
	
	//Redireccionas a la pagina de infolibro con la variable que guarda la referencia
	header("Location: index.php?location=infolibro&Referencia=$ref");
}
include 'view/layout.php';

}else{
	//Rediriges a la pagian de login
	header("Location: index.php?location=login");
}


?>
