<?php 
require_once 'model/Usuario.php';
require_once 'model/Compra.php';

if(count(scandir("./Images"))>2){
	//Inculuimos la libreria de funciones
	include 'LibreriaFunciones.php';

	vaciarCarpeta("./Images");
}
//Si existe la Directorio
if(isset($_SESSION['directorio'])){
	//Borramos directorio
	unset($_SESSION['directorio']);
}

//Si exite un suario conectado	
if (isset($_SESSION['usuario'])) {
	$modifica=false;
	$borra=false;
	$compras=false;


	//Si se pulsa BotonBorrar
	if(isset($_REQUEST['BotonBorrar'])){
		$borra=true;
		
	}

	//Si pulsa borrar
	if(isset($_REQUEST['Borrar'])){
		//Borramos de la base de datos
		Usuario::borrarUsuario($_SESSION['usuario']->getIDUser());
		//Borramos la session
		unset($_SESSION['usuario']);
		//Redireccionas a la pagina de principal del usuario
		header("Location: index.php?location=login");
	}

	//Si pulsa Desactivar
	if(isset($_REQUEST['Desactivar'])){
		//Borramos de la base de datos
		Usuario::activoDesactivo(0,$_SESSION['usuario']->getIDUser());
		//Borramos la session
		unset($_SESSION['usuario']);
		//Redireccionas a la pagina de principal del usuario
		header("Location: index.php?location=login");
	}

	//Si se pulsa BotonModificar
	if(isset($_REQUEST['BotonModificar'])){
		$modifica=true;
	}

	//Si se pulsa BotonNuevo
	if(isset($_REQUEST['BotonNuevo'])){
	//Redireccionas a la pagina de principal del usuario
		header("Location: index.php?location=subir");
	}


	//Si se pulsa BotonCompras
	if(isset($_REQUEST['BotonCompras'])){
		$compras=true;
		$_SESSION['factura']=Compra::listarLibrosComprados($_SESSION['usuario']->getIDUser());
	}

	//Si se pulsa BotonModificar
	if(isset($_REQUEST['Modificar'])){
		
		//Añadir comprabación de las campos
		$modificadoCorrecto=true;
		//Iniciamos libreria de funciones
		include 'LibreriaFunciones.php';

	
		
		//Si el campo de la contraseña esta vacio
		if(validarCampoVacio($_POST['passUp']) == 0){
			//PassUpResumen recoge el valor de la contraseña antigua
			$passUpResumen=$_SESSION['usuario']->getPass();
		}else{
			//Si no recoge la del campo
			$passUpResumen=hash("sha256",$_POST['passUp']);
		}
		
		//Validacion de que el campo  Nombre  no este vacio
		if(validarCampoVacio($_POST['nomUp']) == 0){
			$error['errorNomUp']='Campo Nombre Vacio';
			$modificadoCorrecto=false;
		}else{
			//Validacion de que el campo  Nombre solo sena letras
			if(validarCampoTexto($_POST['nomUp']) == 0){
				$error['errorNomUp']='El campo Nombre solo puede contener letras';
				$modificadoCorrecto=false;
			}
		}
		
		//Validacion de que el campo  Apellidos no este vacio
		if(validarCampoVacio($_POST['apellUp']) == 0){
			$error['errorApellUp']='Campo Apellidos Vacio';
			$modificadoCorrecto=false;
		}else{
			//Validacion de que el campo  Apellidos solo sena letras
			if(validarCampoTexto($_POST['apellUp']) == 0){
				$error['errorApellUp']='El campo Apellidos solo puede contener letras';
				$modificadoCorrecto=false;
			}
		}

		//Validacion de que el campo  Fecha Nacimiento  no este vacio
		if(validarCampoVacio($_POST['FechaNacUp']) == 0){
			$error['errorFechaNacUp']='Campo Fecha Nacimiento esta Vacio';
			$modificadoCorrecto=false;
		}else{
			//Validacion de  que el campo Fecha naciemiento no sea una fecha superior a la actual
			if(validarCampoFecha($_POST['FechaNacUp'])==0){
				
				$error['errorFechaNacUp']='Error formato Fecha "AAAA-MM-DD"';
				$modificadoCorrecto=false;

			}else if(validarCampoFecha($_POST['FechaNacUp'])==2){

				
				$error['errorFechaNacUp']='Fecha Incorrecta';
				$modificadoCorrecto=false;
				
			}else
				if(validarFechaAnt($_POST['FechaNacUp']) == 0){
					
				$error['errorFechaNacUp']='El campo Fecha Nacimiento tiene que ser anterior a la fecha actual';
				$modificadoCorrecto=false;
			}
			
		}

		//Validacion de que el campo  Fecha Nacimiento  no este vacio
		if(validarCampoVacio($_POST['emailUp']) == 0){
			$error['errorEmailUp']='Campo Email esta Vacio';
			$modificadoCorrecto=false;
		}else{
			if(validarEmail($_POST['emailUp']) == 0){
				$error['errorEmailUp']='Formato incorrecto del Email ';
				$modificadoCorrecto=false;
			}
		}

		if($modificadoCorrecto){
			
			//LLamada a la funcion insertarUsuario con los valores de los campos
			$modificado=Usuario::modificarUsuario($passUpResumen,$_POST['nomUp'],$_POST['apellUp'],$_POST['FechaNacUp'],$_POST['emailUp'],$_SESSION['usuario']->getIDUser());
			//Si la funcion devuelve true
			if($modificado){
				//Iniciamos sesion
				$_SESSION['usuario']=Usuario::validarUsuario($_SESSION['usuario']->getIDUser(),$passUpResumen);
				//Redireccionas a la pagina del usuario
				header('Location: index.php?location=paguserperfil&modificaOK');
			//Sino
			}	

		}else{
			$modifica=true;
		}
	}
	//Si se pulsa infolibro
	if(isset($_REQUEST['Cancelar'])){
		//Cambia los valores a false
		unset($_SESSION['modifica']);
		$borra=false;
		$compras=false;
		//Redireccionas a la pagina de principal del usuario
		header("Location: index.php?location=paguserperfil");
	}	
	include 'view/layout.php';

//Si no 
}else{
	//Redireccionas a la pagina del login
	header('Location: index.php?location=login');
}
?>