<?php 
require_once 'model/Libro.php';

if(count(scandir("./Images"))>2){
	//Inculuimos la libreria de funciones
	include 'LibreriaFunciones.php';

	vaciarCarpeta("./Images");
}
//Si existe la Directorio
if(isset($_SESSION['directorio'])){
//Borramos directorio
	unset($_SESSION['directorio']);
}

//Si se pulsa login
if(isset($_REQUEST['login'])){
	//Redireccionas a la pagina de login
	header('Location: index.php?location=login');
}

//Si se pulsa perfil
if(isset($_REQUEST['Perfil'])){
	//Redireccionas a la pagina de perfil 
	header("Location: index.php?location=paguserperfil");
}

//Si se pulsa login
if(isset($_REQUEST['Inicio'])){
	//Redireccionas a la pagina de inicio
	unset($_SESSION['buscar']);
	unset($_SESSION['directorio']);
	header('Location: index.php?location=inicio');
}	

//Si se pulsa PaginaUser
if(isset($_REQUEST['PaginaUser'])){

	//Se borra los elementos de sesion
	unset($_SESSION['buscar']);
	unset($_SESSION['directorio']);

	if($_SESSION['usuario']->getAdmin()==1){

		//Redireccionas a la pagina del usuario
		header('Location: index.php?location=paguserperfil');

	}else{

		//Redireccionas a la pagina del usuario
		header('Location: index.php?location=paguser');
	}
}

//Si se pulsa cerrar
if(isset($_REQUEST['Cerrar'])){

	//Se borra los elementos de sesion
	unset($_SESSION['listaLibrosComprados']);
	unset($_SESSION['buscarLibrosComprados']);
	unset($_SESSION['usuario']);
	unset($_SESSION['directorio']);
	unset($_SESSION['tipoCom']);
	unset($_SESSION['nomCom']);
	unset($_SESSION['usuario']);

	//Redireccionas a la pagina del login
	header('Location: index.php?location=login');
}

//SI se ha pulsado bucar
if(isset($_REQUEST['buscar'])){

	//Se borra los elementos de sesion
	unset($_SESSION['tipo']);

	$_SESSION['nom']=$_POST['nom'];

	//Se realiza una consulta a la funcion buscar libro con el valor de el campo nom y si mete el resultado en la variable de Sesion
	$_SESSION['buscar']=Libro::buscarLibro($_POST['nom'],$_POST['nom'],$_POST['nom']);

	//Redireccionas a la pagina de inicio
	header('Location: index.php?location=inicio');
}

//Al carcar la pagina se realiza una consulta por medio de la funcion listarLibrosNew que te devuelve los libros que se han subido en la ultima semana y se mete en una variable de sesion
$_SESSION['listar']=Libro::listarLibroNew();

//Si se pulsa un genero
if(isset($_GET['Genero'])){

	//Se borra los elementos de sesion
	unset($_SESSION['nom']);

	$_SESSION['tipo']=$_GET['tipo'];
	//Se realiza una consulta a la funcion buscarlibroGeneros con el valor de el campo tipo y si mete el resultado en la variable de Sesion
	$_SESSION['buscar']=Libro::buscarLibroGeneros($_GET['tipo']);
	
	//Redireccionas a la pagina de inicio
	header('Location: index.php?location=inicio');
}

//Si se pulsa infolibro
if(isset($_GET['InfoLibro'])){

	//Guardamos el valor de la referencia en una variable
	$ref=$_GET['Referencia'];
	
	//Redireccionas a la pagina de infolibro con la variable que guarda la referencia
	header("Location: index.php?location=infolibro&Referencia=$ref");
}
//Incluimos la el layout
include 'view/layout.php';
 ?>