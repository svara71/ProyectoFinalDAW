<?php 
require_once 'model/Libro.php';

if(count(scandir("./Images"))>2){
	//Inculuimos la libreria de funciones
	include 'LibreriaFunciones.php';

	vaciarCarpeta("./Images");
}

//Si existe la Directorio
if(isset($_SESSION['directorio'])){
	//Borramos directorio
	unset($_SESSION['directorio']);
}
	
//Si se a iniciado sesion y el usuario es administrador
if(isset($_SESSION['usuario']) && $_SESSION['usuario']->getAdmin()==1){

	//Si existe la referencia
	if(isset($_GET['referencia'])){

		//Recuperamos el valor de la referencia
		$ref=$_GET['referencia'];

		//Usamos la funcion Buscar libro por referencia y metemos los datos en una variable de session
		$correct=Libro::buscarLibroRef($ref);

		if(!is_null($correct)){
			$libro=$correct;
		}else{
			//Redireccionas a la pagina del inicio
			header('Location: index.php?location=inicio');
		}
	}else{
		//Redireccionas a la pagina del inicio
		header('Location: index.php?location=inicio');
	}




	//Si se pulsa infolibro
	if(isset($_POST['atras'])){

	//Guardamos el valor de la referencia en una variable
	$ref=$libro->getRef_Libro();
	
	//Redireccionas a la pagina de infolibro con la variable que guarda la referencia
	header("Location: index.php?location=infolibro&Referencia=$ref");
	}

	
	
	
	
	if(isset($_POST['modificar'])){
		
		//Variable boolean que comprueba que no hay errores en los campos
		$updateLibro=true;

		//Inculuimos la libreria de funciones
		include 'LibreriaFunciones.php';

			//Si el campo autor esta vacio
			if(validarCampoVacio($_POST['nomLibroUp']) == 0){
				//La Variable pasa a false
				$updateLibro=false;
				//Cargamos un mensaje en el error
				$error['errorNomLibroUp']='Campo Autor Vacio';
			}
		
			//Si el campo autor esta vacio
			if(validarCampoVacio($_POST['autorUp']) == 0){
				//La Variable pasa a false
				$updateLibro=false;
				//Cargamos un mensaje en el error
				$error['errorAutorUp']='Campo Autor Vacio';
			}

			//Si el campo precio esta vacio
			if(validarCampoVacio($_POST['precioUp']) == 0){
				//La Variable pasa a false
				$updateLibro=false;
				//Cargamos un mensaje en el error
				$error['errorPrecioUp']='Campo Precio Vacio';
			//si no
			}else{
				//Si el campo precio no es un numero o un numero decimal
				if(validarCampoNumeroDecimal($_POST['precioUp']) == 0){
					//La Variable pasa a false
					$updateLibro=false;
					//Cargamos un mensaje en el error
					$error['errorPrecioUp']='Campo solo numerico decimal(.)';				
				}
			}
		
			//Si el campo numero de páginas esta vacio
			if(validarCampoVacio($_POST['numPagUp']) == 0){
				//La Variable pasa a false
				$updateLibro=false;
				//Cargamos un mensaje en el error
				$error['errorNumPagUp']='Campo Númreo de Páginas Vacio';
			//si no 
			}else{
				//Si el campo no es un numero entero
				if(validarCampoNum($_POST['numPagUp'])== 0){
					//La Variable pasa a false
					$updateLibro=false;
					//Cargamos un mensaje en el error
					$error['errorNumPagUp']='Campo solo numerico ';				
				}
			}


			//Si se a declarado generos
			if(isset($_POST['generosUp'])){
				//Convertimos el array en un string
				$generos=implode ( ',' , $_POST['generosUp']);
				
			//si no
			}else{
				//La Variable pasa a false
				$updateLibro=false;
				//Cargamos un mensaje en el error
				$error['errorGenerosUp']='No se a seleccionado ningun campo';	
			}
			

			if($_FILES['epubUp']['error'] == 0){

				//Si el typo del archivo no coincide o no se a añadido ninguno 
				if(!($_FILES['epubUp']['type']=='application/epub+zip' || $_FILES['epubUp']['type']=='application/octet-stream')){
					//La Variable pasa a false
					$updateLibro=false;
					//Cargamos un mensaje en el error
					$error['errorArchivoUp']='No se a seleccionado archivo epub';
				//si NO
				}
					
			}else if($_FILES['epubUp']['error'] !== 4){
				//La Variable pasa a false
				$updateLibro=false;

				//Cargamos un mensaje en el error
				$error['errorArchivoUp']='Error De subida del archivo'.$_FILES['epubUp']['error'];
			}

		
			
			//Si el Campo sinopsis esta vacio
			if(validarCampoVacio($_POST['sinopsisUp'])==0){
				//La Variable pasa a false
				$updateLibro=false;
				//Cargamos un mensaje en el error
				$error['errorSinopsisUp']='Campo sinopsis Vacio';
				
			}
		
			//Si el campo fecha esta vacio
			if(validarCampoVacio($_POST['fechaPubliUp'])==0){
				//La Variable pasa a false
				$updateLibro=false;
				//Cargamos un mensaje en el error
				$error['errorFechaPubliUp']='Campo Fecha de Publicación Vacio';
			//Si no 
			}else{
				//Si hay un error de formato
				if(validarCampoFecha($_POST['fechaPubliUp'])==0){
					//La Variable pasa a false
					$updateLibro=false;
					//Cargamos un mensaje en el error
					$error['errorFechaPubliUp']='Error formato Fecha "AAAA-MM-DD"';
				//Si hay un error en la fecha tipo (31 de frebrero o mes 13)
				}else if(validarCampoFecha($_POST['fechaPubliUp'])==2){
					//La Variable pasa a false
					$updateLibro=false;
					//Cargamos un mensaje en el error
					$error['errorFechaPubliUp']='Fecha Incorrecta';
				}
			}

			//Variable que almacena la fecha de el dia en que se subio el libro
			$Fecha_Up=date("Y-m-d");
		

			if($updateLibro){
				

				if(($libro->getNom_Libro()!==$_POST['nomLibroUp']) && ($_FILES['epubUp']['error'] == 4 )){

					//Elementos que se eliminan de el titulo
					$buscar = array ("\\","/","*",":","?","¿");

					 //Los remplazamos por vacio 
		    		$UpNomLibro = str_replace($buscar,"",$_POST['nomLibroUp']);

		    		//Cambiamos el nombre de archivo en la carpeta descargas
		    		rename('./Descargar/epub/'.$libro->getNom_Libro().'.epub', './Descargar/epub/'.$UpNomLibro.'.epub');

		    		rename('./libros/'.$libro->getNom_Libro(), './libros/'.$UpNomLibro);
	    			//Cargamos en la variable portada
	    			$portada='./libros/'.$UpNomLibro.'/OEBPS/Images/cover.jpg';
	    			//Cargamos en la variable archivo
					$archivo='./libros/'.$UpNomLibro;	
					//Cargamos en la variable titulo
					$titulo=$UpNomLibro;
					
				}else if(($libro->getNom_Libro()==$_POST['nomLibroUp']) && ($_FILES['epubUp']['error'] == 0)){


					//Borramos el archivo epub de la carpeta descargas
					unlink('./Descargar/epub/'.$libro->getNom_Libro().'.epub');

					//Usuamos una funcion para borrar recursivamenter
					rmDir_rf($libro->getArchivo());

					//Cambiamos el nombre del fichero por el titulo.epub
					$_FILES['epubUp']['name']=$libro->getNom_Libro().'.epub';

					//Movemos el archivo epub a la carpeta de descargas 
					move_uploaded_file($_FILES['epubUp']['tmp_name'],'Descargar/epub/'.$_FILES['epubUp']['name']);

					//Creamos un nuevo ZipArchive
					$zip = new ZipArchive;
					//Abrimos el archivo que pasamos a la carpeta de descargas 
					$zip->open('./Descargar/epub/'.$_FILES['epubUp']['name']);
					//Estraemos en la carpeta libros 
					$zip->extractTo('libros/'.$libro->getNom_Libro());
					//Cerramos ZipArchive
					$zip->close();
					//Cambiamos los permisos de la carpeta que contiene los archivos del la extracion
					chmod('libros/'.$_POST['nomLibro'], 0777);
					//chown('libros/'.$_POST['nomLibro'], 'samu:samu');

					//Cargamos en la variable portada
	    			$portada=$libro->getPortada();
	    			//Cargamos en la variable archivo
					$archivo=$libro->getArchivo();
					//Cargamos en la variable titulo
					$titulo=$libro->getNom_Libro();	

				
				}else if(($libro->getNom_Libro()!==$_POST['nomLibroUp']) && ($_FILES['epubUp']['error'] == 0)){


					//Borramos el archivo epub de la carpeta descargas
					unlink('./Descargar/epub/'.$libro->getNom_Libro().'.epub');

					//Usuamos una funcion para borrar recursivamenter
					rmDir_rf($libro->getArchivo());

					//Elementos que se eliminan de el titulo
					$buscar = array ("\\","/","*",":","?","¿");

					 //Los remplazamos por vacio 
		    		$UpNomLibro = str_replace($buscar,"",$_POST['nomLibroUp']);

					//Cambiamos el nombre del fichero por el titulo.epub
					$_FILES['epubUp']['name']=$_POST['nomLibroUp'].'.epub';

					//Movemos el archivo epub a la carpeta de descargas 
					move_uploaded_file($_FILES['epubUp']['tmp_name'],'Descargar/epub/'.$_FILES['epubUp']['name']);

					//Creamos un nuevo ZipArchive
					$zip = new ZipArchive;
					//Abrimos el archivo que pasamos a la carpeta de descargas 
					$zip->open('./Descargar/epub/'.$_FILES['epubUp']['name']);
					//Estraemos en la carpeta libros 
					$zip->extractTo('libros/'.$_POST['nomLibroUp']);
					//Cerramos ZipArchive
					$zip->close();

					//Cambiamos los permisos de la carpeta que contiene los archivos del la extracion
					chmod('libros/'.$_POST['nomLibro'], 0777);
					//chown('libros/'.$_POST['nomLibro'], 'samu:samu');

					//Cargamos en la variable portada
	    			$portada='./libros/'.$UpNomLibro.'/OEBPS/Images/cover.jpg';
	    			//Cargamos en la variable archivo
					$archivo='./libros/'.$UpNomLibro;	
					//Cargamos en la variable titulo
					$titulo=$UpNomLibro;
					
			}else if(($libro->getNom_Libro()==$_POST['nomLibroUp']) && ($_FILES['epubUp']['error'] == 4)){

					//Cargamos en la variable portada
	    			$portada=$libro->getPortada();
	    			//Cargamos en la variable archivo
					$archivo=$libro->getArchivo();
					//Cargamos en la variable titulo
					$titulo=$libro->getNom_Libro();	
			}


					//LLamamo a la funcion que inserta un nuevo libro en la bae de datos
			
					Libro::modificarLibro($titulo, $_POST['autorUp'], $_POST['numPagUp'], $_POST['fechaPubliUp'], $_POST['sinopsisUp'], $portada, $generos, $Fecha_Up,$archivo, $_POST['precioUp'],$_POST['isbnUp']);

					//Redireccionas a la pagina del login
					header('Location: index.php?location=modlibro&referencia='.$_POST['isbnUp']);

			}

	}

	include 'view/layout.php';

}else{
	//Redireccionas a la pagina del login
	header('Location: index.php?location=login');
}



	
 ?>