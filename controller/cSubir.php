<?php 
require_once 'model/Libro.php';

if(count(scandir("./Images"))>2){
	//Inculuimos la libreria de funciones
	include 'LibreriaFunciones.php';

	vaciarCarpeta("./Images");
}
//Si existe la Directorio
if(isset($_SESSION['directorio'])){
	//Borramos directorio
	unset($_SESSION['directorio']);
}
	
//Si se a iniciado sesion y el usuario es administrador
if(isset($_SESSION['usuario']) && $_SESSION['usuario']->getAdmin()==1){
 
	//Si se pulsa enviar
	if(isset($_POST['enviar'])){
		//Variable boolean que comprueba que no hay errores en los campos
		$insertLibro=true;

		//Inculuimos la libreria de funciones
		include 'LibreriaFunciones.php';

		//Si el campo titulo esta vacio
		if(validarCampoVacio($_POST['nomLibro'])==0){
			//La variable pasa a false
			$insertLibro=false;
			//Si carga un mensaje en el error 	
			$error['errorNomLibroNew']='Campo Titulo Vacio';
		//Si no 
		}else{
			 	//Elementos que se eliminan de el titulo
			   	$buscar = array ("\\","/","*",":","?","¿");
			   	//Los remplazamos por vacio 
    			$newNomLibro = str_replace($buscar,"",$_POST['nomLibro']);

    			//Cargamos en la variable portada
    			$portada='./libros/'.$newNomLibro.'/OEBPS/Images/cover.jpg';
    			//Cargamos en la variable archivo
				$archivo='./libros/'.$newNomLibro;		
		}

		//Si el campo isbn esta vacio
		if(validarCampoVacio($_POST['isbn']) == 0){
			//La variable pasa a false
			$insertLibro=false;
			//Si carga un mensaje en el error 
			$error['errorISBNNew']='Campo ISBN Vacio';
		//Si no
		}else {
			//Si el campo ISBN esta mal formateado
			if(validarCampoISBN($_POST['isbn'])== 0){
				//La Variable pasa a false
				$insertLibro=false;
				//Cargamos un mensaje en el error 
				$error['errorISBNNew']='Error de formato';
				
			}
		}

		//Si el campo autor esta vacio
		if(validarCampoVacio($_POST['autor']) == 0){
			//La Variable pasa a false
			$insertLibro=false;
			//Cargamos un mensaje en el error
			$error['errorAutorNew']='Campo Autor Vacio';
		}

		//Si el campo precio esta vacio
		if(validarCampoVacio($_POST['precio']) == 0){
			//La Variable pasa a false
			$insertLibro=false;
			//Cargamos un mensaje en el error
			$error['errorPrecioNew']='Campo Precio Vacio';
		//si no
		}else{
			//Si el campo precio no es un numero o un numero decimal
			if(validarCampoNumeroDecimal($_POST['precio']) == 0){
				//La Variable pasa a false
				$insertLibro=false;
				//Cargamos un mensaje en el error
				$error['errorPrecioNew']='Campo solo numerico decimal(.)';				
			}
		}
	
		//Si el campo numero de páginas esta vacio
		if(validarCampoVacio($_POST['numPag']) == 0){
			//La Variable pasa a false
			$insertLibro=false;
			//Cargamos un mensaje en el error
			$error['errorNumPagNew']='Campo Númreo de Páginas Vacio';
		//si no 
		}else{
			//Si el campo no es un numero entero
			if(validarCampoNum($_POST['numPag'])== 0){
				//La Variable pasa a false
				$insertLibro=false;
				//Cargamos un mensaje en el error
				$error['errorNumPagNew']='Campo solo numerico ';				
			}
		}


		//Si se a declarado generos
		if(isset($_POST['generos'])){
			
			//Convertimos el array en un string
			$generos=implode ( ',' , $_POST['generos']);
		//si no
		}else{
			//La Variable pasa a false
			$insertLibro=false;
			//Cargamos un mensaje en el error
			$error['errorGenerosNew']='No se a seleccionado ningun campo';	
		}
		
		//Si el typo del archivo no coincide o no se a añadido ninguno 
		if(!($_FILES['epub']['type']=='application/epub+zip' || $_FILES['epub']['type']=='application/octet-stream')){
			//La Variable pasa a false
			$insertLibro=false;
			//Cargamos un mensaje en el error
			$error['errorArchivoNew']='No se a seleccionado archivo epub';
		//si NO
		}else{
			//Cambiamos el nombre del fichero por el titulo introducido mas la extencion
			$_FILES['epub']['name']=$newNomLibro.'.epub';
		}
		
		//Si el Campo sinopsis esta vacio
		if(validarCampoVacio($_POST['sinopsis'])==0){
			//La Variable pasa a false
			$insertLibro=false;
			//Cargamos un mensaje en el error
			$error['errorSinopsisNew']='Campo sinopsis Vacio';
			
		}
	
		//Si el campo fecha esta vacio
		if(validarCampoVacio($_POST['fechaPubli'])==0){
			//La Variable pasa a false
			$insertLibro=false;
			//Cargamos un mensaje en el error
			$error['errorFechaPubliNew']='Campo Fecha de Publicación Vacio';
		//Si no 
		}else{
			//Si hay un error de formato
			if(validarCampoFecha($_POST['fechaPubli'])==0){
				//La Variable pasa a false
				$insertLibro=false;
				//Cargamos un mensaje en el error
				$error['errorFechaPubliNew']='Error formato Fecha "AAAA-MM-DD"';
			//Si hay un error en la fecha tipo (31 de frebrero o mes 13)
			}else if(validarCampoFecha($_POST['fechaPubli'])==2){
				//La Variable pasa a false
				$insertLibro=false;
				//Cargamos un mensaje en el error
				$error['errorFechaPubliNew']='Fecha Incorrecta';
			}
		}

		//Variable que almacena la fecha de el dia en que se subio el libro
		$Fecha_Up=date("Y-m-d");

		//Si insertLibro es true
		if($insertLibro){

					//Movemos el archivo epub a la carpeta de descargas 
					move_uploaded_file($_FILES['epub']['tmp_name'],'Descargar/epub/'.$_FILES['epub']['name']);

					//Creamos un nuevo objeto ZipArchive
					$zip = new ZipArchive;
					//Abrimos el archivo que pasamos a la carpeta de descargas 
					$zip->open('./Descargar/epub/'.$_FILES['epub']['name']);
					//Extraemos en la carpeta libros 
					$zip->extractTo('libros/'.$_POST['nomLibro']);
					//Cerramos ZipArchive
					$zip->close();

					//Cambiamos los permisos de la carpeta que contiene los archivos del la extracion
					chmod('libros/'.$_POST['nomLibro'], 0777);
					//chown('libros/'.$_POST['nomLibro'], 'samu:samu');
					
					//LLamamo a la funcion que inserta un nuevo libro en la bae de datos
					Libro::insertarLibro($_POST['isbn'], $_POST['nomLibro'], $_POST['autor'], $_POST['numPag'], $_POST['fechaPubli'], $_POST['sinopsis'], $portada, $generos, $Fecha_Up,$archivo, $_POST['precio']);
					
		}
			
	}
	//Incluimos el layout
	include 'view/layout.php';
//si no 
}else{
	//Redireccionas a la pagina del login
	header('Location: index.php?location=login');
}
?>
