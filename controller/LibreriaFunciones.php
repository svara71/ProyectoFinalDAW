<?php 
	/**
	 * 
	 * Libreria de funciones 
	 * 
	 * 
	 * Libreria de funciones para la validación de los campos de un formulario
	 * 
	 *  @author Samuel Vara García 
	 *  @version 1.0.0
	 * 
	 * 
	 * 
	 * */

		/**
		 * 
		 * borrar recursivamente y borrar la carpeta
		 * 
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
	 	 * 
		 * 
		 * 
		 * */
		  function rmDir_rf($carpeta)
		  {
		      foreach(glob($carpeta . "/*") as $archivos_carpeta){             
		        if (is_dir($archivos_carpeta)){
		          rmDir_rf($archivos_carpeta);
		        } else {
		        unlink($archivos_carpeta);
		        }
		      }
		      rmdir($carpeta);
     	}


		/**
		 * 
		 * borrar el contenido de una carpeta
		 * 
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
	 	 * 
		 * 
		 * 
		 * */
		  function vaciarCarpeta($carpeta)
		  {
		      foreach(glob($carpeta . "/*") as $archivos_carpeta){             
		        if (is_dir($archivos_carpeta)){
		          rmDir_rf($archivos_carpeta);
		        } else {
		        unlink($archivos_carpeta);
		        }
		      }
		     
     	}

		/**
		 * 
		 * Función que valida un campo de texto
		 * 
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
	 	 * 
		 * @param string texto que se quiere comprobar
		 * 
		 * @return int valido_texto  cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */
		function validarCampoTexto($texto){
			$valido_texto = 0;
			$patron_texto = "/^[a-zA-ZáéíóúAÉÍÓÚÑñ ]+$/";//Patron para validar el texto
			
				if(preg_match($patron_texto, $texto)){//Si la cadena que le pasas cumple con el patron 
					$valido_texto  = 1;//El valor de validar_texto cambia a 1
					return $valido_texto ;
				}else{//Si no lo cumple se mantiene en 0
					return $valido_texto ;
				}
		
		}

		/**
		 * 
		 * Función que valida un campo email
		 * 
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
	 	 * 
		 * @param string email que se quiere comprobar
		 * 
		 * @return int valido cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */
		function validarEmail($email){
			$valido= 0;
			$patron_email = "/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/";//Patron para validar el texto
			
				if(preg_match($patron_email, $email)){//Si la cadena que le pasas cumple con el patron 
					$valido  = 1;//El valor de validar_texto cambia a 1
					return $valido ;
				}else{//Si no lo cumple se mantiene en 0
					return $valido ;
				}
		
		}

		/**
		 * 
		 * Función que valida un campo numerico
		 * 
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
	 	 * 
		 * @param string num que se quiere comprobar
		 * 
		 * @return int valido  cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */
		function validarCampoNum($num){
			$valido = 0;
			$patron_num = "/^[0-9]+$/";//Patron para validar el texto
			
				if(preg_match($patron_num, $num)){//Si la cadena que le pasas cumple con el patron 
					$valido = 1;//El valor de validar_texto cambia a 1
					return $valido;
				}else{//Si no lo cumple se mantiene en 0
					return $valido;
				}
		
		}

		/**
		 * 
		 * Función que valida si un campo esta vacio
		 * 
		 *  @author Samuel Vara García 
		 *  @version 1.0.0
		 * 
		 * @param string texto que se quiere comprobar
		 * 
		 * @return int valido_texto  cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */

		function validarCampoVacio($texto){
			$valido_texto = 0;
			
			
				if(empty( $texto)){//Si la cadena que le pasas esta vacia
					
					return $valido_texto ;
				}else{//Si no 
					
					$valido_texto  = 1;//El valor de validar_texto cambia a 1
					return $valido_texto ;
				}
		
		}

		/**
		 * 
		 * Función que valida si un campo de fecha
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
		 * @param string fecha que se quiere comprobar
		 * 
		 * @return int valido_fecha  cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */
		function validarCampoFecha($fecha){
			$valido_fecha = 0;
			
			if (preg_match("/^(\d{4})\-(\d{2})\-(\d{2})/", $fecha)) {//Si se cumple el patron cambia a 1 
					$ArrayFecha = explode("-",$fecha);//Divide la fecha pasada y la combierte en un Array 
			
					//Recoge los valores del dia el mes y el año del array
				     $dia=$ArrayFecha[2];
				   	 $mes=$ArrayFecha[1];
				     $ano=$ArrayFecha[1];
	    		
	    		if(checkdate($mes,$dia,$ano)){
				           $valido_fecha  = 1;
	    				    return $valido_fecha ;
				}else{
					$valido_fecha  = 2;
					return $valido_fecha ;
				}
					    		

			}else{//Si no lo cumple se mantiene en 0
					
				return $valido_fecha ;
			}
		}
		/**
		 * 
		 * Función que valida si un campo alfanumerico
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
		 * 
		 * @param string alfanum que se quiere comprobar
		 * 
		 * @return int valido_alfanum cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */
		function validarCampoAlfanumerico($alfnum){
			$valido = 0;
			$patron_alfanum ="/^[0-9a-zA-ZáéíóúAÉÍÓÚÑñ ]+$/";//Patron para validar un campo con texto y numeros
			
				if(preg_match($patron_alfanum, $alfnum)){//Si la cadena que le pasas cumple con el patron 
					$valido = 1;//El valor de validar_alfanum cambia a 1
					return $valido;
				}else{//Si no lo cumple se mantiene en 0
					return $valido;
			
				}
	
		}

		/**
		 * 
		 * Función que valida si un el campo es un ISBN correcto
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
		 * 
		 * @param string isbn que se quiere comprobar
		 * 
		 * @return int valido cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */
		function validarCampoISBN($isbn){
			$valido = 0;
			$patron_isbn ="/^(\d{3})(\-)(\d{2})(\-)(\d{4})(\-)(\d{3})(\-)(\d{1})/";//Patron para validar un campo con texto y numeros
			
				if(preg_match($patron_isbn, $isbn)){//Si la cadena que le pasas cumple con el patron 
					$valido = 1;//El valor de validar_alfanum cambia a 1
					return $valido;
				}else{//Si no lo cumple se mantiene en 0
					return $valido;
			
				}
	
		}

		/**
		 * 
		 * Función que valida si un campo numero
		 * 
		 *  @author Samuel Vara García 
		 *  @version 1.0.0
		 * 
		 * @param string numero que se quiere comprobar
		 * 
		 * @return int valido_numero cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */
		function validarCampoNumeroDecimal($numero){
			$valido_numero = 0;
			$patron_numero = "/^-?[0-9]+([,\.][0-9]*)?$/";//Patron para validar un campo con texto y numeros
			
				if(preg_match($patron_numero, $numero)){//Si la cadena que le pasas cumple con el patron 
					$valido_numero = 1;//El valor de validar_numero cambia a 1
					return $valido_numero;
				}else{//Si no lo cumple se mantiene en 0
					return $valido_numero;
			
				}
	
		}




		/**
		 * 
		 * Función que un campo fecha sea anterior al dia de hoy
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
		 * 
		 * @param string fecha que se quiere comprobar
		 * 
		 * @return int valido_fecha cambia a uno si es correcto se mantiene en 0 si hay error
		 * 
		 * 
		 * */
		function validarFechaAnt($fecha){
			$valido_fecha = 0;
			$ArrayFecha = explode("-",$fecha);//Divide la fecha pasada y la combierte en un Array 
			
			//Recoge los valores del dia el mes y el año del array
			 $ano=$ArrayFecha[0];
			 $mes=$ArrayFecha[1];
		     $dia=$ArrayFecha[2];
		 
		     
	
			//Recoge los valores del dia actual
			$diahoy=date("d");
			$meshoy=date("m");
			$anohoy=date("Y");
			
				//Compara que la fecha pasada no sea superior al la fecha actual
				if($ano = $anohoy && $mes = $meshoy && $dia <= $diahoy || $ano = $anohoy && $mes < $meshoy  || $ano < $anohoy  ){//Si es anterior a la fecha devuelve 1

					$valido_fecha = 1;
					return $valido_fecha;
					
				}else{//Si no devuelve 0
					
					$valido_fecha = 0;
					return $valido_fecha;
				}
	
				
		}
		
		/**
		 * 
		 * Función que calcula la edad mediante un afecha
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
		 * 
		 * @param string fecha que se quiere comprobar
		 * 
		 * @return int edad con la edad caculada
		 * 
		 * 
		 * */	
		function CalculaEdad( $fecha ) {
			$Array = explode("/",$fecha);//Divide la fecha pasada y la combierte en un Array 
			
			//Recoge los valores del dia el mes y el año del array		  
		    $dia =$Array[0];
		    $mes =$Array[1];
		   	$ano =$Array[2];
			
			//Recoge los valores del dia actual		  
			$diahoy=date("d");
			$meshoy=date("m");
			$anohoy=date("Y");
			
					if (($mes == $meshoy) && ($dia > $diahoy)) {
						$anohoy=($anohoy-1); }

						//si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual

						if ($mes > $meshoy) {
						$ano=($ano-1);}

						//ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad

						$edad=($anohoy-$ano);
						
						return $edad;
		}


		/**
		 * 
		 * Función que valida una contraseña
		 * 
		 *  @author Samuel Vara García 
	 	 *  @version 1.0.0
		 * 
		 * @param string contraseña que se quiere comprobar
		 * 
		 * @return int validar-pass con la edad caculada
		 * 
		 * 
		 * */	
		function validarCampoPass($pass)
		{
			$valido_pass = 0;

			if(strlen($pass) < 8)
			{
				$valido_pass = 1;//El valor de validar_numero cambia a 1
				return $valido_pass;
			}
			else if(!preg_match('/(\d)/', $pass))//Si no tine esos caracteres 
			{
				$valido_pass = 1;//El valor de validar_numero cambia a 1
				return $valido_pass;
			}
			else if(!preg_match('/([a-z])/', $pass)) //Si no tine esos caracteres
			{
				$valido_pass = 1;//El valor de validar_numero cambia a 1
				return $valido_pass;
			}
			else if(!preg_match('/([A-Z])/', $pass))//Si no tine esos caracteres 
			{
				$valido_pass = 1;//El valor de validar_numero cambia a 1
				return $valido_pass;
			}
			
			return $valido_pass;
		}
		
		function desconexion(){
			// Recuperamos la información de la sesión 
			session_start(); 
			// Y la eliminamos 
			session_destroy(); 
			header("Location: Actividad-03-LogoIN.php"); 
		}
?>