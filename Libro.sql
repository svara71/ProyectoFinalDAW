
CREATE TABLE `Libro` (
  `Ref_Libro` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Nom_Libro` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Autor` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Num_Pag` int(4) NOT NULL,
  `Fecha_Publi` date NOT NULL,
  `Sinopsis` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Portada` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Generos` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Fecha_Up` date NOT NULL,
  `Archivo` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Precio` float NOT NULL
   PRIMARY Key(`Ref_Libro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT INTO `libro` (`Ref_Libro`, `Nom_Libro`, `Autor`, `Num_Pag`, `Fecha_Publi`, `Sinopsis`, `Portada`, `Generos`, `Fecha_Up`, `Archivo`, `Precio`) VALUES
(' 978-84-9674-816-3', 'La princesa de hielo', 'Camilla LÃ¤ckberg', 376, '2003-01-01', 'Tras muchos aÃ±os de ausencia, la joven escritora Erica vuelve a su pueblo natal, donde ha heredado la casa de sus padres, recientemente fallecidos. Durante un paseo por las calles donde transcurrieron los primeros aÃ±os de su vida, tras el aviso de unos vecinos, descubre que su amiga de la infancia, Alex, acaba de suicidarse. Conmocionada, inicia una investigaciÃ³n y descubre que Alex estaba embarazada. La historia da un nuevo giro cuando la autopsia revela que su amiga no se suicidÃ³ sino que fue asesinada. La policÃ­a detiene al principal sospechoso, Anders, un artista fracasado que mantenÃ­a una relaciÃ³n especial con la vÃ­ctima.', './libros/La princesa de hielo/OEBPS/Images/cover.jpg', 'InvestigaciÃ³n, Policial', '2017-11-20', './libros/La princesa de hielo', 3.79),
('978-84-0809-623-8', 'Cazadores de Sombras Los Origenes 1 Angel Mecanico', 'CASSANDRA CLARE', 448, '2010-08-31', 'Tessa Gray estÃ¡ dispuesta a encontrar a su hermano, del que no recibe noticias desde hace tiempo. Para ello, se dirige a Londres, donde serÃ¡ raptada por las Hermanas Oscuras, miembros de una organizaciÃ³n secreta llamada el Club Pandemonium, y rescatada por los Cazadores de Sombras. Tessa se sentirÃ¡ atraÃ­da en seguida por Jem y Will, y deberÃ¡ elegir quiÃ©n de ellos ganarÃ¡ su corazÃ³n mientras los tres siguen en busca de su hermano y descubren que alguien trama acabar con ellos.', './libros/Cazadores de Sombras Los Origenes 1 Angel Mecanico/OEBPS/Images/cover.jpg', 'FantasÃ­a, novela de aventuras y literatura juvenil', '2017-08-11', './libros/Cazadores de Sombras Los Origenes 1 Angel Mecanico', 9.9),
('978-84-2534-883-9', 'Cincuenta Sombras de Grey', ' E. L. James ', 540, '2011-06-20', 'Cuando la estudiante de literatura Anastasia Steele recibe el encargo de entrevistar al exitoso y joven empresario Christian Grey, queda impresionada al encontrarse ante un hombre atractivo, seductor y tambiÃ©n muy intimidante. La inexperta e inocente Ana intenta olvidarle, pero pronto comprende cuÃ¡nto le desea.\r\n\r\nGrey estÃ¡ atormentado por sus propios demonios y le consume la necesidad de controlarlo todo, pero a su vez se ve incapaz de resistirse a la serena belleza de Ana, a su inteligencia y a su espÃ­ritu independiente. Debe admitir que la desea, pero bajo ciertas condiciones.\r\n\r\nCuando la pareja por fin inicia una apasionada relaciÃ³n, las peculiares prÃ¡cticas erÃ³ticas de Grey desconciertan a Ana, al tiempo que ella descubre los lÃ­mites de sus propios y mÃ¡s oscuros deseos...', './libros/Cincuenta Sombras de Grey/OEBPS/Images/cover.jpg', 'ErÃ³tica,RomÃ¡ntica', '2017-11-22', './libros/Cincuenta Sombras de Grey', 9.99),
('978-84-7888-445-2', 'Harry Potter y la piedra filosofal', 'J.K. Rowling', 256, '1997-06-30', 'Harry Potter se ha quedado huÃ©rfano y vive en casa de sus abominables tÃ­os y del insoportable primo Dudley. Harry se siente muy triste y solo, hasta que un buen dÃ­a recibe una carta que cambiarÃ¡ su vida para siempre. En ella le comunican que ha sido aceptado como alumno en el colegio interno Hogwarts de magia y hechicerÃ­a. A partir de ese momento, la suerte de Harry da un vuelco espectacular. En esa escuela tan especial aprenderÃ¡ encantamientos, trucos fabulosos y tÃ¡cticas de defensa contra las malas artes. Se convertirÃ¡ en el campeÃ³n escolar de quidditch, especie de fÃºtbol aÃ©reo que se juega montado sobre escobas, y se harÃ¡ un puÃ±ado de buenos amigos... aunque tambiÃ©n algunos temibles enemigos. Pero sobre todo, conocerÃ¡ los secretos que le permitirÃ¡n cumplir con su destino. Pues, aunque no lo parezca a primera vista, Harry no es un chico comÃºn y corriente. Â¡Es un verdadero mago', './libros/Harry Potter y la Piedra Filosofal/OEBPS/Images/cover.jpg', 'FantasÃ­a, novela de aventuras y literatura juvenil', '2017-08-27', './libros/Harry Potter y la Piedra Filosofal', 6.8),
('978-84-9066-043-0', 'Hombres Sin Mujeres', 'Haruki Murakami', 272, '2014-01-01', 'En su obra mÃ¡s reciente, Haruki Murakami ofrece a los lectores siete relatos en torno al aislamiento y la soledad que preceden o siguen a la relaciÃ³n amorosa: hombres que han perdido a una mujer, o cuya relaciÃ³n ha estado marcada por el desencuentro, asisten inermes al regreso de los fantasmas del pasado, viven el enamoramiento como una enfermedad letal, son incapaces de establecer una comunicaciÃ³n plena con la pareja, o ven extraÃ±amente interrumpida su historia de amor. Otros experimentan atormentados amores no correspondidos o, incluso, como en el relato protagonizado por una metamorfosis kafkiana, desconocen todavÃ­a los mecanismos del afecto y del sexo. Sin embargo, las verdaderas protagonistas de estos relatos â€”llenos de guiÃ±os a los Beatles, el jazz, Kafka, Las mil y una noches o, en el caso del tÃ­tulo, Hemingwayâ€”, son ellas, las mujeres, que, misteriosas, irrumpen en la vida de los hombres para desaparecer, dejando una huella imborrable en la vida de aquellos que las han amado, o de los que, al menos, intentaron amarlas.\r\n', './libros/Hombres Sin Mujeres/OEBPS/Images/cover.jpg', 'Sociedad', '2017-11-22', './libros/Hombres Sin Mujeres', 7.49),
('978-84-9759-379-3', 'It', 'Stephen King', 1504, '1986-09-15', 'Â¿QuiÃ©n o quÃ© mutila y mata a los niÃ±os de un pequeÃ±o pueblo norteamericano? Â¿Por quÃ© llega cÃ­clicamente el horror a Derry en forma de un payaso siniestro que va sembrando la destrucciÃ³n a su paso? Esto es lo que se proponen averiguar los protagonistas de esta novela. Tras veintisiete aÃ±os de tranquilidad y lejanÃ­a una antigua promesa infantil les hace volver al lugar en el que vivieron su infancia y juventud como una terrible pesadilla. Regresan a Derry para enfrentarse con su pasado y enterrar definitivamente la amenaza que los amargÃ³ durante su niÃ±ez. Saben que pueden morir, pero son conscientes de que no conocerÃ¡n la paz hasta que aquella cosa sea destruida para siempre. It es una de las novelas mÃ¡s ambiciosas de Stephen King, donde ha logrado perfeccionar de un modo muy personal las claves del gÃ©nero de terror.', './libros/It/OEBPS/Images/cover.jpg', 'Terror', '2017-11-22', './libros/It', 7.99);


CREATE TABLE `Usuario` (
  `IDUser` varchar(25) Primary key CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Pass` varchar(256) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Nom` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Apell` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Fecha_Nac` date NOT NULL,
  `Email` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Activo` tinyint(1) NOT NULL DEFAULT '1',
  `Admin` tinyint(1) NOT NULL DEFAULT '0'
   PRIMARY Key(`IDuser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `usuario` (`IDUser`, `Pass`, `Nom`, `Apell`, `Fecha_Nac`, `Email`, `Activo`, `Admin`) VALUES
('Admin', '4dd09b8f659e27847f94782920fb7e41b2c5afbd7f419a4a3ed8ab7aa5b7f944', 'Admin', 'Admin', '2017-11-20', 'Admin@Admin.com', 1, 1),
('patri', '4dd09b8f659e27847f94782920fb7e41b2c5afbd7f419a4a3ed8ab7aa5b7f944', 'patri', 'patri', '2017-11-01', 'patri', 1, 0),
('Samu', '4dd09b8f659e27847f94782920fb7e41b2c5afbd7f419a4a3ed8ab7aa5b7f944', 'Samuel', 'Vara GarcÃ­a', '1995-06-08', 'svara71@gmail.com', 1, 0);


CREATE TABLE `Compra` (
  `Ref_Compra` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Ref_Libro` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `IDUser` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Fecha_Compra` date DEFAULT NULL,
    FOREIGN KEY (`Ref_Libro`) REFERENCES Libro(`Ref_Libro`)
    on DELETE CASCADE
    on UPDATE CASCADE,
    FOREIGN KEY (`IDUser`) REFERENCES Usuario(`IDUser`)
    on DELETE CASCADE
    on UPDATE CASCADE,
    PRIMARY Key(`Ref_Compra`,`Ref_Libro`,`IDuser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT INTO `compra` (`Ref_Compra`, `Ref_Libro`, `IDUser`, `Fecha_Compra`) VALUES
(3, ' 978-84-9674-816-3', 'Samu', '2017-11-20'),
(4, '978-84-7888-445-2', 'Samu', '2017-11-20');